# README #

## Quick summary
- A [sam](https://9fans.github.io/plan9port/man/man1/sam.html) like editor by [Nim](https://nim-lang.org/)
- Built-in forth interpreter based on [zforth](https://github.com/zevv/zForth.git).
- Built-in Japanese input method Kana-Kanji translation [skk](http://openlab.ring.gr.jp/skk/index-j.html)

## Version
- 0.1.0

## Summary of set up
1. [install Nim](https://nim-lang.org/install.html)
    - nimble install ned (not still available)
1. or
    - git clone <URL>
    - cd nimedit
    - nimble install
1. Dependencies
    - require iconv if use skk
    - optional plan9port

## Repo owner or admin
- Yoshinori Arai [mail](kumagusu08@gmail.com)
