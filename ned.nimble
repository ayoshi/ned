# Package

version       = "0.1.0"
author        = "Yoshinori Arai"
description   = "A simple console editor of nim"
license       = "GPL"
srcDir        = "src"
bin           = @["ned"]

# Dependencies

requires "nim >= 0.17.3"
