import os, osproc, posix

const
  TMPDIR* = "/tmp"
  SHELL* = "/bin/sh"
  HOME* = "HOME"
  PNAME* = "ned"
  #PLAN9DIR = "/usr/local/plan9"
  #PLAN9RC = PLAN9DIR & "/bin/rc"

proc getUser*(): string =
  result = ""
  if existsEnv("LOGNAME"):
    result = getEnv("LOGNAME")
  else:
    result = "nobody"

#proc getHome(): string =
#  getHomeDir()

#proc getTemp(): string =
#  getTempDir()

proc getCwd(): string =
  getCurrentDir()

proc tempFile(): string =
  #let tmp = getTemp()
  let pid = getpid()
  let user = getUser()
  result = "X" & $pid & "." & user & "_" & PNAME

proc makeTempFile*(src: string): string =
  var tmp = tempFile()
  let user = getUser()
  let tmpdir = getTempDir() & PNAME & '_' & user
  if not existsDir(tmpdir):
    try:
      createDir(tmpdir)
    except OSError:
      echo "OS error: can't create tmp directory"
      return ""
  tmp = tmpdir & '/' & tmp
  copyFile(src, tmp)
  setFilePermissions(tmp, {fpUserRead, fpUserWrite})
  return tmp

proc okDir*(path: string): bool =
  var dir = $dirname(path)
  result = existsDir(dir)

proc tilde*(path: string): string =
  expandTilde(path)

   
when isMainModule:
  echo "user => ", getUser()
  #echo "home dir => ", getHome()
  echo "temp dir => ", getTemp()
  echo "current dir => ", getCwd()
  echo "temp file => ", tempFile()
  echo "make temp file => ", makeTempFile("unix.nim")
  #echo "fileinfo waldn10.txt => ", repr(fileInfo("waldn10.txt"))

#  for k, v in envPairs():
#    echo "key => ", k, " value => ", v
  echo "okDir /home/yoshi/nim/nimedit/src/unix.nim ", okDir("/home/yoshi/nim/nimedit/src/unix.nim")
  echo "okDir ~/nim/nimedit/src/unix.nim ", okDir("~/nim/nimedit/src/unix.nim")
  echo "tilda ~/nim/nimedit/src/unix.nim ", tilde("~/nim/nimedit/src/unix.nim")
