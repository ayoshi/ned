import os, sequtils, strutils, streams, times, unicode
from pegs import Peg, peg
import piece, unix
import log

when defined(testing):
  const DEBUG = true
else:
  const DEBUG = false

const Module = "file"

# trigger to call resetFile
const
  MaxPieceLen = 10000
  MaxAddText = 4096 * 16

type
  Range* = tuple[p1: int, p2: int]
  FileStat = tuple[name: string, path: string, size: int, mtime: int64]
  File* = ref object of RootObj # piece.Contents
    fstat*: FileStat
    fs*: FileStream      # stream connect to original content never change
    ss*: string          # only append additional string
    piece: piece.Piece
    dot*: Range
    ndot*: Range
    mark*: Range   # range set by command k
    lastpat*: tuple[re: string, pe: Peg]  # last regexp string and peg
    sel*: seq[Range]  # regexp selections
    modified*: bool
    unread*: bool
    closeok*: bool
    deleted*: bool
    tmp*: string
    uname*: string
    cache*: string
    lines: seq[Range]
    rsize: int
    cacheok*: bool
    lastindex*: tuple[idx: int, offset: int]      # for address.indexToOffset, offsetToIndex
  FileList* = seq[File]

const
  Clean = ' '
  Dirty = '\''
  Unread = '-'
  Current = '*'

var curfile*: File = nil
var filelist*: FileList = newSeq[File]()

# depend on curfile must to be link curent file object
proc getOrig(x, y: int, f: File = curfile): string =
  setPosition(f.fs, x)
  readStr(f.fs, y - x + 1)

proc getAdds(x, y: int, f: File = curfile): string =
  f.ss[x .. y]

# initialize File obj only don't connect any stream
proc initFile*(fname: string = "noname"): File =
  var path: string
  var fsize: int
  var mtime: int64
  if fname != "noname" and existsFile(fname):
    path = expandFilename(fname)
    fsize = cast[int](getFileSize(fname))
    mtime = toUnix(getLastModificationTime(fname))
  else:
    path = getCurrentDir() & '/' & fname
    fsize = 0
    mtime = toUnix(getTime())
  let fstat = (name: fname, path: path, size: fsize, mtime: mtime)
  let p = initPiece(fsize)
  let f = File(fstat: fstat,
               fs: nil,
               ss: "",
               piece: p,
               dot: (0, 0),
               ndot: (0, 0),
               mark: (0, 0),
               lastpat: (re: "", pe: peg("nil")),
               sel: newSeq[Range](),
               modified: false,
               unread: true,    # fs, ss are connect to stream then unread is false?
               closeok: true,
               deleted: false,
               tmp: "",
               uname: "",
               cache: "",
               lines: newSeq[Range](),     # line (p1, p2) cache
               rsize: 0,
               cacheok: false,
               lastindex: (0, 0))
  return f

proc getFileName*(): string =
  if curfile != nil: return curfile.fstat.name
  else: return ""

proc getFilePath*(): string =
  if curfile != nil: return curfile.fstat.path
  else: return ""
  
# set File to FileList
proc setFileList*(f: File) =
  filelist.add(f)

# get File object from FileList
proc getFileObj*(name: string): File =
  for f in items(filelist):
    if f.fstat.name == name:
      return f
  return nil

# set curent file
proc setCurFile*(f: File) =
  curfile = f

proc setCurFile*(name: string) =
  let f = getFileObj(name)
  setCurFile(f)

# delete File obj from FileList
proc okDelete*(f: var File): bool =
  if f.closeok:
    return true
  elif f.modified and not f.closeok:
    echo "?changes to ", f.fstat.name
    f.closeok = true
    return false

proc deleteFileFromList*(name: string) =
  var i = 0
  if len(filelist) == 0: return
  else:
    var f = getFileObj(name)
    if okDelete(f):
      f.deleted = true

# file state utilities
proc setFileTogleClean*(f: var File) =
  if not f.modified:
    f.modified = true
    f.closeok = false
  else:
    f.modified = false
    f.closeok = true

proc setFileTogleClean(name: string) =
  var f = getFileObj(name)
  setFileTogleClean(f)

proc setFileTogleUnread(f: var File) =
  if f.unread: f.unread = false
  else: f.unread = true

proc setFileTogleUnread(name: string) =
  var f = getFileObj(name)
  setFileTogleUnread(f)

proc fileState*(f: File): string =
  result = ""
  if not f.modified: result = result & Clean
  else: result = result & Dirty
  if f.unread: result = result & Unread
  else: result = result & '+'
  if curfile == f: result = result & Current
  else: result = result & ' '

proc showFileList*() =
  for f in items(filelist):
    if not f.deleted:
      echo fileState(f) & ' ' & f.fstat.name

# set file name for 'f' command. can undo set file name
proc setFileName*(name: string, f: var File) =
  if f.fstat.name == "noname":
    f.fstat.name = name
    f.fstat.path = replace(f.fstat.path, "noname", name)
  else:
    let uname = f.fstat.name
    f.fstat.name = name
    f.fstat.path = replace(f.fstat.path, uname, name)
    if f.uname == "":
      f.uname = uname
    else:
      f.uname = f.uname & '/' & uname

# undo set file name
proc unsetFileName(f: var File) =
  let names = split(f.uname, '/')
  if names != @[""]:
    let prevname = f.fstat.name
    f.fstat.name = names[high(names)]
    let pos = rfind(f.uname, '/')
    if pos != -1:
      f.uname = f.uname[0 .. pos - 1]
    else:
      f.uname = prevname
    f.fstat.path = replace(f.fstat.path, prevname, names[high(names)])

# set dot
proc setDot*(p1, p2: int, f: var File) =
  f.dot = (p1: p1, p2: p2)

## load file: connect File obj to Orig(FileStream) and Add(String)
##
## call after initialize File obj by initFile then hold original contents by f.tmp
## that is connect to file stream(f.fs) and hold additional string by f.ss
##
## File obj will be reset by triggers below that will be checked when call snapshot
##       * number of piece item(piece table size) is than 300
##       * length of additional string(f.ss) is than 4096 byte
##
proc loadFile*(f: var File): bool =
  var tmp: string
  result = true
  if f.fs != nil or len(f.ss) != 0 or existsFile(f.tmp):
    echo "already loaded"
    return false
  if f.fstat.name != "noname" and f.fstat.size != 0:
    try:
      tmp = makeTempFile(f.fstat.name)
      f.fs = openFileStream(tmp)
    except OSError:
      echo "OS error: can't set file permission"
      result = false   # set permission correctly if OS error occur?
    except IOError:
      echo "IO error: can't open file stream"
      result = false
    except:
      echo "Unknowm error: can't load file"
      result = false
    finally:
      f.tmp = tmp
  if f.unread: setFileTogleUnread(f)
  f.ss = ""

# close File: close FileStream and delete tmp file
proc closeFile*(f: var File) =
  if f.fs != nil:
    close(f.fs)
    try:
      removeFile(f.tmp)
    except OSError:
      echo "cna't remove temporaly file."

## resetFile
## Warning! after reset File, history reseted of course.
proc resetFile(s: string, f: File) =
  # close file stream
  close(f.fs)
  
  # overwrite content to f.tmp
  var sf: system.File
  if open(sf, f.tmp, fmWrite):
    try:
      write(sf, s)
    except IOError:
      echo "IO error!"
    except:
      echo "Unknown exception!"
      raise
    finally:
      close(sf)

  # reopen file stream and connect to f.fs
  f.fs = openFileStream(f.tmp)

  # change other field data of File
  f.ss = ""
  f.piece = initPiece(s.len)

# make line cache
proc makeLines(s: string): seq[Range] =
  result = newSeq[Range]()
  var offset = 0
  for l in splitLines(s, keepEol = true):
    let rl = runeLen(l)
    result.add((p1: offset, p2: offset + rl))
    offset += rl
    
# snapshot: return current content
proc snapshot*(f: File): string =
  var reset = false
  result = ""

  # call resetFile or not
  if len(f.piece.table) > MaxPieceLen or len(f.ss) > MaxAddText: reset = true

  if f.cacheok: result = f.cache
  else:
  # concat all piece
    for pi in items(f.piece.table):
      when DEBUG: debugLog(Module, "file: " & $pi.file & " offset: " & $pi.offset & " len: " & $pi.len)
      if pi.file == Orig:
        result = result & getOrig(pi.offset, pi.offset + pi.len - 1, f)
      else: result = result & getAdds(pi.offset, pi.offset + pi.len - 1, f)
    f.cache = result
    f.lines = makeLines(result)
    f.rsize = runeLen(result)
    f.cacheok = true
    f.lastindex = (idx: 0, offset: 0)

    # call resetFile
    if reset: resetFile(result, f)

proc rsnapshot*(f: File): seq[Rune] =
  if f.cacheok: result = toRunes(f.cache)
  else:
    let s = snapshot(f)
    result = toRunes(s)

# TODO
proc snapshot*(x, y: int, f: File): string =
  let s = snapshot(f)
  return s[x..y]
    
proc rsnapshot*(x, y: int, f: File): seq[Rune] =
  let s = rsnapshot(f)
  result = newSeq[Rune]()
  for i in x..y: result.add(s[i])

# editor command and utilities
# file size
proc fileSize*(f: File): int =
  if f.cacheok: result = f.cache.len
  else:
    let s = snapshot(f)
    result = s.len

# file size by rune
proc rfileSize*(f: File): int =
  if f.cacheok: result = f.rsize
  else:
    let s = rsnapshot(f)
    result = s.len
 
# return last line number
proc lastLine*(f: File): int =
  let s = snapshot(f)
  result = countLines(s)

proc lastLine*(s: string): int =
  result = countLines(s)

# getLine
proc getLine*(lnum: int, f: File): string =
  var ln = 1
  let s = snapshot(f)
  for l in splitLines(s):
    if ln == lnum:
      result = l
      break
    inc(ln)

# append string
proc appendStr*(offset: int, s: string, f: var File) =
  piece.doInsert(offset, s, f.ss, f.piece)
  if not f.modified: setFileTogleClean(f)
  f.cacheok = false

# delete string
proc deleteStr*(offset: int, len: int, f: var File, replace = false) =
  piece.doDelete(offset, len, f.piece, replace)
  if not f.modified: setFileTogleClean(f)
  f.cacheok = false

# replace string
proc replaceStr*(offset: int, len: int, s: string, f: var File) =
  piece.doReplace(offset, len, s, f.ss, f.piece)
  if not f.modified: setFileTogleClean(f)
  f.cacheok = false

# offset to line number
proc offsetToLine*(offset: int, f: File): int =
  var ln = 1
  var last = 0
  let s = rsnapshot(f)
  if offset < 0 or s.len < offset:
    return -1
  while true:
    if last == offset: break
    if isWhiteSpace(s[last]) and toUTF8(s[last])[0] == '\n': inc(ln)
    inc(last)
  return ln

# line number to offset
#proc lineToRange*(lnum: int, f: File): Range =
#  var ln = 1
#  var last: string
#  var offset = 0
#  let s = snapshot(f)
#
#  if lnum < 0 or lastLine(s) < lnum:
#    return (-1, -1)
#  if lnum == 0: return (0, 0)
#  for l in splitLines(s, keepEol = true):
#    last = l
#    if ln == lnum: break
#    offset += runeLen(l)
#    inc(ln)
#  return (offset, offset + runelen(last))
  
proc lineToRange*(lnum: int, f: File): Range =
  if not f.cacheok: discard snapshot(f)
  return f.lines[lnum - 1]
    
proc writeLine*(lnum: int, f: File) =
  write(stdout, getLine(lnum, f))
  write(stdout, '\n')

# write whole contents for test code
proc writeAll*(f: File) =
  let s = snapshot(f)
  write(stdout, s)

# save file
proc saveFile*(path: string, f: var File, startp = -1, endp = -1): int =
  var sf: system.File
  let s = snapshot(f)
  if open(sf, path, fmWrite):
    try:
      if startp == -1 and endp == -1:
        write(sf, s)
        result = runeLen(s)
      else:
        let ep = if endp == file.fileSize(f): endp - 1 else: endp
        write(sf, s[startp .. ep])
        result = runeLen(s[startp .. ep])
    except IOError:
      echo "IO error!"
    except:
      echo "Unknown exception!"
      raise
    finally:
      close(sf)

# undo/redo
proc undoFile*(cmdc: char, cnt: int, f: var File) =
  var i = cnt
  when DEBUG: debugLog(Module, "** enter undoFile **")
  case cmdc:
  of 'a', 'c', 'i', 'd', 's', 'm', 't', 'r', '<', '|':
    if cmdc == 'm': i *= 2    # move: append and delete
    if i > 0:
      while i > 0:
        piece.doUndo(f.piece)
        dec(i)
    else:
      while i < 0:
        piece.doRedo(f.piece)
        inc(i)
    f.cacheok = false      # need it!
  of 'f':
    unsetFileName(f)
  else: discard
  
# test code
when isMainModule:
#[
  block filelist:
    type
      TestFiles = array[0 .. 3, string]
    var
      f: File
      files: TestFiles
    files = ["foo", "test.txt", "piece.nim", "nimedit.nim"]
    for fn in items(files):
      f = initFile(fn)
      setFileList(f)
    echo "** file list test **"
    setCurFile("piece.nim")
    echo "curfile => ", curfile.fstat.name
    showFileList()
    echo "-- change --"
    setCurFile("foo")
    echo "curfile => ", curfile.fstat.name
    showFileList()
    echo "delete from list; foo"
    deleteFileFromList("foo")
    showFileList()
    echo "set file modified to true"
    setFileTogleClean("piece.nim")
    showFileList()
    echo "delete from list: piece.nim"
    deleteFileFromList("piece.nim")
    showFileList()
    echo "again delete: piece.nim"
    deleteFileFromList("piece.nim")
    showFileList()

  block appendDeleteReplace:
    echo "** append test **"
    var f: File
    f = initFile("test.txt")
    discard loadFile(f)
    setCurFile(f)
    echo "-- append 'NIMEDIT' at 0 --"
    appendStr(0, "NIMEDIT\n", f)
    #echo "file obj test.txt => ", repr(f)
    writeAll(f)
    echo "-- append 'NIMEDIT' at 56 --"
    appendStr(56, "NIMEDIT ", f)
    writeAll(f)
    #echo "file obj => ", repr(f)
    echo "-- delete 1st line --"
    deleteStr(0, 8, f)
    writeAll(f)
    echo "-- delete at 48 len 8 --"
    deleteStr(48, 8, f)
    writeAll(f)
    echo "-- replace 1st line with 'NIMEDIT'--"
    replaceStr(0, 66, "NIMEDIT\n", f)
    writeAll(f)
    echo "-- replace 4th line with 'NIMEDIT'--"
    replaceStr(137, 63, "NIMEDIT\n", f)
    writeAll(f)
    #echo "file obj => ", repr(f)

  block getlineSnapshot:
    var f = initFile("waldn10.txt")
    setFileList(f)
    #echo "FIle: ", repr(f)
    if loadFile(f):
      setCurFile(f)
    echo "-- File list --"
    showFileList()

    echo "file name => ", curfile.fstat.name
    echo "file size => ", curfile.fstat.size
    echo "file last modification time => ", curfile.fstat.mtime
    echo "5000: ", getLine(5000, f)
    echo "5001: ", getLine(5001, f)
    echo "5002: ", getLine(5002, f)
    echo "#315153, #315217: ", snapshot(315153, 315217, f)
    echo "delete 315153 len ", 315217 - 315153, ": "
    deleteStr(315153, 315217 - 315153, f)
    echo "#315153, #315217: ", snapshot(315153, 315217, f)

  block offsetToLineLineToRangeSaveFile:
    var f = initFile("waldn10.txt")
    setFileList(f)
    #echo "FIle: ", repr(f)
    if loadFile(f):
      setCurFile(f)
    echo "#0 => ", offsetToLine(0, f)
    echo "#2 => ", offsetToLine(2, f)
    #echo "#315153 => ", offsetToLine(315153, f)
    echo "#310893 => ", offsetToLine(310893, f)
    #assert(offsetToLine(315893, f) == 5000, "offsetTo Range failed!")
    echo "1 => ", lineToRange(1, f)
    echo "2 => ", lineToRange(2, f)
    #echo "300 => ", lineToRange(300, f)
    echo "5000 => ", lineToRange(5000, f)
    assert(lineToRange(5000, f) == (310892, 310965), "lineNumToRange failed!")

    let ss = snapshot(f)
    assert(lastLine(f) == 10505, "lastLine failed!")
    assert(lastLine(ss) == 10505, "lastLine failed!")

    #echo "rfileSize => ", rfileSize(f)
    assert(rfileSize(f) == 656206, "rfileSize failed!")
    
    echo "saveFile foo: "
    let wsize = saveFile("waldn10.bak", f)
    echo "waldn10.bak: #", wsize

  #echo "findstr \"Thoreau\": ", findStr("Thoreau", 0, 0, f)
  #echo "readBlock(0, 512, f):", readBlock(offset = 0, f = f)

  var f = initFile()
  if loadFile(f):
    setFileName("foo", f)
    echo "noname => ", repr(f)
    setFileName("baz", f)
    echo "foo => ", repr(f)
    unsetFileName(f)
    echo "unset baz => ", repr(f)

 block lineToRangeTimeTest:
    var f = initFile("waldn10.txt")
    setFileList(f)
    #echo "FIle: ", repr(f)
    if loadFile(f):
      setCurFile(f)
    echo "lineToRange old"
    var t0 = cpuTime()
    echo "1 => ", lineToRange(1, f)
    echo "2 => ", lineToRange(2, f)
    echo "300 => ", lineToRange(300, f)
    echo "5000 => ", lineToRange(5000, f)
    echo "time => ", cpuTime() - t0
    echo "lineToRange new"
    t0 = cpuTime()
    echo "1 => ", lineToRange2(1, f)
    echo "2 => ", lineToRange2(2, f)
    echo "300 => ", lineToRange2(300, f)
    echo "5000 => ", lineToRange2(5000, f)
    echo "time => ", cpuTime() - t0
]#
  block resetFileTest:
    var f = initFile("test.txt")
    setFileList(f)
    #echo "FIle: ", repr(f)
    if loadFile(f): setCurFile(f)
    # append string 
    let s = readFile("COPYING")
    appendStr(fileSize(f), s, f)
    echo "before reset f => ", repr(f)
    # take snapshot
    discard snapshot(f)
    echo "after reset f => ", repr(f)
