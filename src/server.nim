import net, threadpool
export Socket

# same as skkserver default port
const 
  port = 1178
  MAXLEN = 1024

#var data: FlowVar[string]
proc send*(client: Socket, data: string)

# receive data from gui part
proc receive(client: Socket, handler: proc (client: Socket, data: string)) {.thread.} =
  var data = ""
  while true:
    try:
      data = recvLine(client)
    except OSError:
      echo "OS error!"
      break
    except TimeoutError:
      echo "time out!"
      break
    handler(client, data)
    
    # Debug code
    echo "Receive: got data => ", repr(data)
    #if data == "bye": break
       
  client.close()
    
# send output data to gui part
proc send*(client: Socket, data: string) =
  var sdata = newString(MAXLEN)
  var dlen = data.len
  var sent = 0

  while sent < dlen:
    if dlen - sent >= MAXLEN:
      while sent <= MAXLEN:
        sdata[sent] = data[sent]
        inc(sent)
    else:
      while dlen - sent > 0:
        sdata[sent] = data[sent]
        inc(sent)
    if trySend(client, sdata):
      echo "send data => ", sdata
    else:
      echo "failed to send data"

# server main function, accept connection from gui part
# then start receive thread
proc nedServer*(handler: proc (client: Socket, data: string)): tuple[client: Socket, address: string] =
  var socket = newSocket()
  socket.bindAddr(Port(port))
  socket.listen()

  var client: Socket
  var address = ""
  socket.acceptAddr(client, address)
  echo "Client connected from: ", address
  
  # start thread
  spawn receive(client, handler)
  
  return (client, address)
   
when isMainModule:
  #import parse, command
  
  proc handle(client: Socket, data: string) =
    if data.len != 0:
      echo "Received data => ", repr(data)
      send(client, data)
    
  let (client, address) = nedServer(handle)
  var i = 1
  while true: inc(i)
    
   
