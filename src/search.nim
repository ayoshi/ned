import pegs, sequtils
import file
import log

when defined(testing):
  const DEBUG = true
else:
  const DEBUG = false
  
const Module = "search"

# search forward r in s from pos
#proc execute*(sel: var seq[file.Range], start: int, sub: string, r: string): bool =
proc execute*(f: var file.File, start: int, sub: string, r: string): bool =
  var matches = newSeq[string]()
  var offset, frt, lst: int
  var ra: file.Range
  var pat: Peg

  result = true

  if f.lastpat.re != r:
    try:
      pat = peg(r)     # TODO: enclose with try block for pegError
    except EInvalidPeg:
      echo "Invalid peg: " & getCurrentExceptionMsg()
      return false
    except:
      echo "Unknown eception: " & getCurrentExceptionMsg()
      return false
  else: pat = f.lastpat.pe

  if f.sel.len != 0: f.sel.delete(0, high(f.sel))
  offset = 0
  while offset < sub.len:
    (frt, lst) = findBounds(sub, pat, matches, offset)
    if frt != -1:
      ra = (p1: frt + start, p2: lst + start)
      f.sel.add(ra)
      if frt + lst == -1: break     # result is offset 0 == '^'
      offset = lst + 1
    else: break
  if f.sel.len == 0: result = false
  f.lastpat = (re: r, pe: pat)

#proc execute*(sel: var seq[file.Range], s: string, r: string, start: int, endp: int): bool =
#  when DEBUG: debugLog(Module, "s.len => " & $s.len & " start: " & $start & " endp: " & $endp)
#  if start > endp:
#    echo "?search"
#    return false
#  if endp == s.len:
#    return executeH(sel, start, s[start .. endp - 1], r)
#  else:
#    return executeH(sel, start, s[start .. endp], r)
  
# search backward r in s from pos
#proc bexecute*(sel: var seq[file.Range], sub: string, r: string): bool =
proc bexecute*(f: var file.File, sub: string, r: string): bool =
  var matches = newSeq[string]()
  var offset, frt, lst: int
  var ra: file.Range
  var pat: Peg

  result = true

  if f.lastpat.re != r:
    try:
      pat = peg(r)     # TODO: enclose with try block for pegError
    except EInvalidPeg:
      echo "Invalid peg: " & getCurrentExceptionMsg()
      return false
    except:
      echo "Unknown eception: " & getCurrentExceptionMsg()
      return false
  else: pat = f.lastpat.pe

  result = true

  if f.sel.len != 0: f.sel.delete(0, high(f.sel))
  offset = 0
  while offset < sub.len :
    (frt, lst) = findBounds(sub, pat, matches, offset)
    if frt != -1:
      ra = (p1: frt, p2: lst)
      f.sel.insert(ra, 0)
      offset = lst + 1
    else: break
  if f.sel.len == 0: result = false
  f.lastpat = (re: r, pe: pat)

#proc bexecute*(sel: var seq[file.Range], s: string, r: string, start: int): bool =
#  return bexecuteH(sel, s[0 .. start], r)

# file match
proc fileMatch*(re: string, f: file.File): bool =
  let pat = peg(re)
  let fname = file.fileState(f) & f.fstat.name
  return match(fname, pat)

# test code
when isMainModule:
  var f = initFile("waldn10.txt")
  var s: string
  #var sel: seq[file.Range]
  var r = "Thoreau"

  if file.loadFile(f):
    setCurFile(f)
    s = file.snapshot(f)
  #if execute(f, 0, s, r):
  if bexecute(f, s[0..426], r):
    echo "sel => ", repr(f.sel)
    echo "lastpat => ", repr(f.lastpat)
  else:
    echo "?no match"

  f = file.initFile("test.txt")
  echo """file match 'test.txt \w+'.txt'' => """, fileMatch("""\W+\w+'.txt'""", f)
  file.setFileTogleClean(f)
  echo "file list => "
  file.showFileList()
  echo "file match modified ''' => ", fileMatch("\'", f)
