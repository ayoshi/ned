import logging

#const DEBUG = true

const
  DefaultLogfile = "ned.log"
  DebugLogfile = "ned.debug"
  customFmtStr = "$levelname, [$datetime]: "
  
var L = newConsoleLogger()
var fL = newFileLogger(DefaultLogfile, fmtStr = customFmtStr)
var dL = newFileLogger(DebugLogfile, fmtStr = customFmtStr)

addHandler(L)
addHandler(fL)
addHandler(dL)

proc debugLog*(module, msg: string) =
  log(dL, lvlDebug, ["(", module, ")-- ", msg])

proc echoLog*(lvl: Level, module, msg: string) =
    log(L, lvl, ["(", module, ")-- ", msg])
    log(fL, lvl, ["(", module, ")-- ", msg])

# test code
when isMainModule:
  #info(Editor & ": logging module")
  #warn(Forth & ": logging module")
  #error(Skk & ": logging module")
  #fatal(Editor & ": logging module")
  echoLog(lvlInfo, "log", "test")
  debugLog("log", "test")
  
