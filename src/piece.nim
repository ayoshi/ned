import sequtils
import log

when defined(testing):
  const DEBUG = true
else:
  const DEBUG = false

const Module = "piece"

#
# Piece Type Definitions
#
type
  # Piece
  Which* = enum Orig, Add
  PieceItem = tuple[file: Which, offset: int, len: int]

  # History for undo/redo
  Ope = enum del, add   #, change
  Hitem = tuple[action: Ope, idx: int, origP: PieceItem, next: bool]

  # Combine Piece and History
  Piece* = object of RootObj
    table*: seq[PieceItem]
    history: seq[Hitem]
    histptr: int
    undoptr: int
    #pcache*: tuple[loffset: int, idx: int]

when isMainModule:
  type
    # file contents: minimal FIle object
    Contents* = object of RootObj
      orig*: proc(x, y: int): string   # TaintedString
      adds*: proc(x, y: int): string   # string

#
# Basic piece operations
#
proc makePiece(file: Which, offset: int, len: int): PieceItem =
  var p: PieceItem
  p = (file: file, offset: offset, len: len)
  return p

proc addPiece(p: PieceItem, piece: var Piece) =
  piece.table.add(p)

proc insertPiece(p: PieceItem, idx: int, piece: var Piece) =
  piece.table.insert(p, idx)

proc deletePiece(idx: int, piece: var Piece) =
  piece.table.delete(idx)

# initialize piece object
proc initPiece*(origLen: int): Piece =
  var p = Piece(table: newSeq[PieceItem](),
                history: newSeq[Hitem](),
                histptr: 0,
                undoptr: 0)
  if origLen != 0:     # origLen == 0: new file
    p.table.add(makePiece(Orig, 0, origLen))
  return p

#
# Piece utilitis
#
proc pieceValues*(idx: int, piece: Piece): (Which, int, int) =
  var
    pi: PieceItem = piece.table[idx]
  result = (pi.file, pi.offset, pi.len)

proc lOffset*(idx: int, piece: Piece): int =
  var
    i: int = 0
  result = 0
  while i < idx:
    result = result + piece.table[i].len
    inc(i)

proc contentsSize*(piece: Piece): int =
  result = 0
  for pi in items(piece.table):
    result = result + pi.len

# find piece by offset: return (idx, logical offset)
proc findPiece*(offset: int, piece: Piece): (int, int) =
  var i = 0
  var l: int
  let
    pi = piece.table[0]
  if offset < pi.len:
    return (0, 0)
  for p in items(piece.table):
    l = l + p.len
    if l > offset:
      return (i, l - p.len)
    inc(i)

#
# history
#

# Basic history operations
proc makeHitem(action: Ope, idx: int, origp: PieceItem, nxt: bool): Hitem =
  (action: action, idx: idx, origP: origp, next: nxt)

proc hitemValues(h: Hitem): (Ope, int, PieceItem, bool) =
  (h.action, h.idx, h.origP, h.next)

# TODO: need var histptr, history
proc addHistory(item: Hitem, piece: var Piece) =
  if piece.undoptr != piece.histptr:
    # delete history until top
    while piece.undoptr < piece.histptr:
      piece.history.delete(piece.histptr)
      dec(piece.histptr)
  piece.history.add(item)
  inc(piece.histptr)
  inc(piece.undoptr)

#
# Edit piece table operation
#

# split piece at the offset
proc splitPiece(idx: int, lo: int, offset: int, len: int, piece: var Piece, replace = false) =
  var
    pi: PieceItem = piece.table[idx]
    np: PieceItem
    (f, o, l) = pieceValues(idx, piece)
  # chaneg => del + add
  addHistory(makeHitem(del, idx, pi, true), piece)
  piece.table[idx].len = offset - lo             # change original piece length
  pi = piece.table[idx]
  addHistory(makeHitem(add, idx, pi, true), piece)
  # insert new piece
  # DEBUG: echo "file: ", f, " offset: ", o + offset - lo + len, " len: ", l - offset + lo - len
  np = makePiece(f, o + offset - lo + len, l - offset + lo - len)
  if replace or len == 0:     # replace or next insert piece
    addHistory(makeHitem(add, idx + 1, np, true), piece)
  else:
    addHistory(makeHitem(add, idx + 1, np, false), piece)
  insertPiece(np, idx + 1, piece)

# delete string from offset to len
proc doDelete*(offset: int, len: int, piece: var Piece, replace = false) =
  var
    p: PieceItem
    o, l: int

  if len <= 0:
    return

  # case 1 begining of contents
  #
  #     piece: tbl[0]    tbl[1]
  #     <++++++|++++><++++++++++++++++>
  #     0    ->|    |       ->|
  #          len    |       len
  #  tbl[0].len - len
  #
  if offset == 0:
    when DEBUG: debugLog(Module, "Delete case 1")
    p = piece.table[0]
    let (_, o, l) = pieceValues(0, piece)
    if l > len:
      # change piece.table[0]
      addHistory(makeHitem(del, 0, p, true), piece)
      piece.table[0].offset = o + len
      piece.table[0].len = l - len
      p = piece.table[0]
      if replace:
        addHistory(makeHitem(add, 0, p, true), piece)
      else:
        addHistory(makeHitem(add, 0, p, false), piece)
    else:
      addHistory(makeHitem(del, 0, p, true), piece)
      deletePiece(0, piece)
      if replace:
        doDelete(0, len - l, piece, true)
      else:
        doDelete(0, len - l, piece)
  # case 2 end of contents
  #
  #  tbl[high(tbl)-1]       tbl[high(tbl)]
  #  +++++++++++++++++><+++++++++++++++++> End of Contents
  #       |<-          |       |<-
  #       len          |<-     len
  #                    tbl[high(tbl)].len - len
  #
  elif offset + len == contentsSize(piece):
    when DEBUG: debugLog(Module, "Delete case 2 offset: " & $offset & " len: " & $len & " contentsSize: " & $contentsSize(piece))
    p = piece.table[high(piece.table)]
    l = p.len
    if l > len:
      addHistory(makeHitem(del, high(piece.table), p, true), piece)
      piece.table[high(piece.table)].len = l - len
      p = piece.table[high(piece.table)]
      if replace:
        addHistory(makeHitem(add, high(piece.table), p, true), piece)
      else:
        addHistory(makeHitem(add, high(piece.table), p, false), piece)
    else:
      addHistory(makeHitem(del, high(piece.table), p, true), piece)
      deletePiece(high(piece.table), piece)
      if replace:
        doDelete(offset, len - l, piece, true)
      else:
        doDelete(offset, len - l, piece)
  # case 3 between stop and end piece
  # tbl[i-1]           tbl[i]               tbl[i+1]
  # +++++++++><++++++++++++++++++++++++><++++++++++++
  #                |<-          ->|         ->|
  #           offset            len         len
  #           <++++>  split piece <++++>
  #
  else:
    when DEBUG: debugLog(Module, "Delete case 3")
    let (i, lo) = findPiece(offset, piece)
    p = piece.table[i]
    o = p.offset
    l = p.len
    if i > 0 and offset == lo:
      when DEBUG: debugLog(Module, "Delete case 3 - 1")
      if l > len:
        when DEBUG: debugLog(Module, "Delete case 3 - 1.1")
        addHistory(makeHitem(del, i, p, true), piece)
        piece.table[i].offset = o + len
        piece.table[i].len = l - len
        p = piece.table[i]
        if replace:
          addHistory(makeHitem(add, i, p, true), piece)
        else:
          addHistory(makeHitem(add, i, p, false), piece)
      else:
        when DEBUG: debugLog(Module, "Delete case 3 - 1.2")
        addHistory(makeHitem(del, i, p, true), piece)
        deletePiece(i, piece)
        if replace:
          doDelete(offset, len - l, piece, true)
        else:
          doDelete(offset, len - l, piece)
    elif lo + l > offset + len:
      when DEBUG: debugLog(Module, "Delete case 3 - 2")
      if replace:
        splitPiece(i, lo, offset, len, piece, true)
      else:
        splitPiece(i, lo, offset, len, piece)
    else:
      when DEBUG: debugLog(Module, "Delete case 3 - 3")
      addHistory(makeHitem(del, i, p, true), piece)
      piece.table[i].len = offset - lo
      p = piece.table[i]
      addHistory(makeHitem(add, i, p, true), piece)
      if replace:
        doDelete(offset, len - l, piece, true)
      else:
        doDelete(offset, len - l, piece)

# insert string with len at offset
proc doInsert*(offset: int, str: string, buf: var string, piece: var Piece) =
  var p: PieceItem
  p = makePiece(Add, buf.len, str.len)
  buf = buf & str

  if offset == 0:                         # begining of contents
    when DEBUG: debugLog(Module, "Insert case 1")
    addHistory(makeHitem(add, 0, p, false), piece)
    insertPiece(p, 0, piece)
  elif offset == contentsSize(piece):   # end of contents
    when DEBUG:debugLog(Module, "Insert case 2 offset => " & $offset & "piece table index => " & $(high(piece.table) + 1))
    addHistory(makeHitem(add, high(piece.table) + 1, p, false), piece)
    addPiece(p, piece)
  else:                       # insert before or split piece and insert
    when DEBUG: debugLog(Module, "Insert case 3")
    let (i, lo) = findPiece(offset, piece)
    #when DEBUG: echo "idx: ", i, " lo(i-1): ", lo
    if i > 0 and offset == lo:
      when DEBUG: debugLog(Module, "Insert case 3 - 1")
      addHistory(makeHitem(add, i, p, false), piece)
      insertPiece(p, i, piece)
    else:
      when DEBUG: debugLog(Module, "Insert case 3 - 2")
      splitPiece(i, lo, offset, 0, piece)
      addHistory(makeHitem(add, i + 1, p, false), piece)
      insertPiece(p, i + 1, piece)
       
# replace string = delete and insert
proc doReplace*(offset: int, len: int, str: string, buf: var string, piece: var Piece) =
  doDelete(offset, len, piece, true)
  doInsert(offset, str, buf, piece)

# undo/redo
proc doUndo*(piece: var Piece) =
  var
    hi: Hitem
    stop: bool = false
  while piece.undoptr != 0:
    hi = piece.history[piece.undoptr - 1]

    if not hi.next:
      if stop: break
      else: stop = true

    if hi.action == del:
      let (_, i, op, _) = hitemValues(hi)
      when DEBUG: debugLog(Module, "undo del i: " & $i & " op: " & $op)
      if i == len(piece.table):
        when DEBUG: debugLog(Module, "undo => add")
        addPiece(op, piece)   # use addPiece when delete last piece!
      else:
        when DEBUG: debugLog(Module, "undo => insert")
        insertPiece(op, i, piece)
    else:
      let (_, i, _, _) = hitemValues(hi)
      when DEBUG: debugLog(Module, "add i: " & $i & " undo => del")
      deletePiece(i, piece)
    dec(piece.undoptr)

proc doRedo*(piece: var Piece) =
  var
    hi: Hitem
    #stop: bool = false

  if piece.undoptr == piece.histptr: return  # no undo history

  while piece.undoptr < piece.histptr:
    hi = piece.history[piece.undoptr]

    if hi.action == del:
      let (_, i, _, _) = hitemValues(hi)
      when DEBUG: debugLog(Module, "redo del i: " & $i)
      deletePiece(i, piece)
    else:
      let (_, i, op, _) = hitemValues(hi)
      when DEBUG: debugLog(Module, "redo add i: " & $i)
      if i == len(piece.table):
        addPiece(op, piece)
      else:
        insertPiece(op, i, piece)  # use addPiece when add last piece

    if hi.next == false: break
    inc(piece.undoptr)
  # need increase undoptr
  if piece.undoptr < piece.histptr: inc(piece.undoptr)

# test code
when isMainModule:
  const
    content = "line1\nline2\nline3\nline4\nline5\n"
  var
    p: Piece = initPiece(content.len)
    adds: string = ""
    contents: Contents

  proc getOrig(x, y: int): string =
    content[x .. y]
  proc getAdds(x, y: int): string =
    adds[x .. y]

  contents.orig = getOrig
  contents.adds = getAdds
  
  proc writeTest(p: Piece) =
    for pi in items(p.table):
      if pi.file == Orig:
        write(stdout, contents.orig(pi.offset, pi.offset + pi.len - 1))
      else:
        write(stdout, contents.adds(pi.offset, pi.offset + pi.len - 1))

  echo "piece: ", repr(p)
  echo "=========="

  writeTest(p)
  echo "=========="
  doDelete(6, 6, p)
  echo "Delete 6, 6: "
  #echo repr(p)
  assert(loffset(1, p) == 6, "loffset failed!")
  writeTest(p)
  echo "=========="
  dodelete(10, 7, p)
  echo "Delete 10, 7: "
  #echo repr(p)
  assert(loffset(2, p) == 10, "loffset failed!")
  writeTest(p)

  echo "=========="
  doInsert(0, "add1\n", adds, p)
  echo "Insert \"add1\" at 0: "
  #echo repr(p)
  writeTest(p)

  echo "=========="
  doReplace(11, 4, "replaced", adds, p)
  echo "Replace 11, 4 with \"replaced\": "
  #doDelete(11, 4, p)
  #echo "Delete 11, 4: "
  #echo repr(p.table)
  writeTest(p)

  echo "=========="
  doInsert(26, "add2\n", adds, p)
  echo "Add \"add2\n\" at 26: "
  #echo repr(p.table)
  writeTest(p)

  echo "=========="
  doUndo(p)
  echo "undo 1:"
  writeTest(p)

  #echo repr(p.table)

  echo "=========="
  doUndo(p)
  echo "undo 2:"
  writeTest(p)

  echo "=========="
  doUndo(p)
  echo "undo 3:"
  writeTest(p)

  echo "=========="
  doUndo(p)
  echo "undo 4:"
  writeTest(p)

  echo "=========="
  doUndo(p)
  echo "undo 5:"
  writeTest(p)
  #echo repr(p)

  echo "=========="
  doRedo(p)
  echo "redo 1:"
  writeTest(p)

  echo "=========="
  doRedo(p)
  echo "redo 2:"
  writeTest(p)

  echo "=========="
  doRedo(p)
  echo "redo 3:"
  writeTest(p)

  echo "=========="
  doRedo(p)
  echo "redo 4:"
  writeTest(p)

  echo "=========="
  doRedo(p)
  echo "redo 5:"
  writeTest(p)
  #echo repr(p)
