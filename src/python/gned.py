# -*- coding:utf-8 -*-
"""
    simple text editor built on python/tkinter
    by Yoshinori Arai (kumagusu08@gmail.com) 2018
    for ned (sam like editor by Nim) gui part
"""
 
import os
import sys
from contextlib import closing
 
try:
    # Python 3
    import tkinter as tk
    from tkinter import scrolledtext

except ImportError:
    # Python 2
    import Tkinter as tk
    import ScrolledText
 
from pygments.lexers.python import PythonLexer
from pygments.lexers.special import TextLexer
#from pygments.lexers.html import HtmlLexer
#from pygments.lexers.html import XmlLexer
#from pygments.lexers.templates import HtmlPhpLexer
#from pygments.lexers.perl import Perl6Lexer
#from pygments.lexers.ruby import RubyLexer
#from pygments.lexers.configs import IniLexer
#from pygments.lexers.configs import ApacheConfLexer
#from pygments.lexers.shell import BashLexer
#from pygments.lexers.diff import DiffLexer
#from pygments.lexers.dotnet import CSharpLexer
#from pygments.lexers.sql import MySqlLexer
#
from pygments.styles import get_style_by_name
import commandmixin as cmd
#import skkazik

class CommandWindow(cmd.CommandMixin, scrolledtext.ScrolledText):
    '''A text widget that accept ned command'''

    def __init__(self, parent, *args, **kwargs):
        # msgwindow
        try:
            self._msgwindow = kwargs.pop('msgwindow')
        except KeyError:
            self._msgwindow = None

        # textview window
        try:
            self._textview = kwargs.pop('textview')
        except KeyError:
            self._textview = None
        else:
            self._textview.tag_configure('DOT', background = 'deep sky blue')

        # list window
        try:
            self._listwindow = kwargs.pop('listwindow')
        except KeyError:
            self._listwindow = None
            
        # initialize CommandMixin 
        self._init()

        # command window widget
        scrolledtext.ScrolledText.__init__(self, parent, *args, **kwargs)
         
        # command prompt
        self.prompt1 = '> '
        self.prompt2 = ': '
        self.prompt = self.prompt1
         
        # command line number
        self.line = 0

        # command history list
        self.history = []

        # command history pointer
        self.historyp = 0
       
        # kind of received data and contents length
        self.datakind = ''
        self.contentslen = 0

        # Current data
        self.data = ''

        # File selection
        self.selectedfile = ''
        
        # current dot(selection of textview)
        self.dot = []       # [char1, char2]
 
        # skk mode: Hira, Kata, Zenei, Koho
        self.skk = 'Hira'

        # skk buffer
        self.skkbuf = ''

        # henkan start mark
        #self.henkanmark = 'Henkan'

        # koho list
        self.koho = []

        # koho list index
        self.kohop = 0

        # connect nedserver then make binding and show prompt
        self.connectNed()
        if self.socket is not None:
            # binding
            self.bind('<KeyRelease-Return>', self.readLine)
            self.bind('<Up>', self.historyUp)
            self.bind('<Down>', self.historyDown)
            self.bind('<Any-KeyPress>', self.skkinput)
            self.showPrompt(self.prompt)

            # start thread
            recvThr = Thread(target=self.recvData)
            recvThr.start()
#        print(len(self.data))
#        print(self._textview)
        if len(self.data) != 0 and self._textview is not None:
            self._textview.delete('1.0', 'end')
            self._textview.insert('end', self.data)
#        self.bind('<Return>', self.test)
#
#    def test(self, event=None):
#        data = sys.argv
#        for i in range(1, len(sys.argv)):
#            self.handleData(data[i])
            

    def handleData(self, data):
        '''Handle received data, output to text viewer or display error'''
        # Debug code
        #print('Received data: %s' % data)
        if data == 'q':
            self.on_closing()

        # Data Protocol
        # [kind]: [body]
        # MESSAGE: data ------ error message
        # RANGE: data -------- dot
        # FILES: data -------- file list
        # CONTENTS: length --- start contents accumulation
        # other: contents data

        # data length < 1024
        idx = data.find('\x00')
        if idx != -1:
          data = data[0:idx]
          #print('Data: %s' % data)

        # set datakind
        if len(self.datakind) == 0:
            list = data.split(' ')
            #print(list)
            self.datakind = list[0]

        if self.datakind == 'MESSAGE:':
            self._msgwindow.delete(0, 'end')
            self._msgwindow.insert(0, list[1:])
            self.datakind = ''
        elif self.datakind == 'RANGE:' or self.datakind == 'FILES:':
            self.showDotOrFilelist(list[1:])
            self.datakind = ''
        elif self.datakind == 'CONTENTS:':
            #print(list[1])
            self.contentslen = int(list[1])
            self.data = ''
            self.datakind = 'DUMMY'
            print('Contents length: ', self.contentslen)
        elif self.datakind == 'DUMMY':
            if len(data) != 0: print('Data: %s, len: %d' % (data, len(data.encode('utf8'))))
            self.data += data
            #print('data len: %d, contentslen: %d' % (len(self.data), self.contentslen))
            if len(self.data.encode('utf8')) == self.contentslen:
                self._textview.delete('1.0', 'end')
                self._textview.insert('end', self.data)
                self.datakind = ''

    def showPrompt(self, prompt):
        self.insert('end', prompt)
        self.line += 1
        
    def readLine(self, event=None):
        cmd = self.get(str(self.line) + '.2', str(self.line) + '.2 lineend')
        print('cmd: %s' % cmd.encode('utf-8'))
        if cmd.find('{') >= 0: self.prompt = self.prompt2
        if cmd.find('}') >= 0: self.prompt = self.prompt1
        self.showPrompt(self.prompt)
        self.history.append(cmd)
        if self.historyp != len(self.history) - 1:
            self.historyp = len(self.history) - 1
        self.historyp += 1
        #print('historyp: %d, history: %s' % (self.historyp, self.history))
        self.sendData(cmd)

    def historyUp(self, event=None):
        try:
            cmd = self.history[self.historyp - 1]
        except IndexError:
            cmd = ''
        if len(cmd) != 0:
            self.delete(str(self.line) + '.2', str(self.line) + '.2 lineend')
            self.insert('end', cmd)
            self.historyp -= 1
        else:
            self.historyp = len(self.history) - 1
        return "break"

    def historyDown(self, event=None):
        try:
            cmd = self.history[self.historyp - 1]
        except IndexError:
            cmd = ''
        if len(cmd) != 0:
            self.delete(str(self.line) + '.2', str(self.line) + '.2 lineend')
            self.insert('end', cmd)
            self.historyp += 1
        else:
            self.historyp = len(self.history) - 1
        return "break"
       
    def dot2index(self, p):
        l = 1
        c = 0
        #print('p: %d' % p)
        if p == 0: return (1, 0)
        for line in self.data.splitlines(True):
            if c > p: break
            c += len(line)
            l += 1
        return (l, c - p)
        
    def showDotOrFilelist(self, data):
        if len(data) == 1 and len(data[0].split(',')) == 2:    # range data
            if self._msgwindow is not None:
                self._msgwindow.delete(0, 'end')
                self._msgwindow.insert(0, 'Range: (%s)' % data[0])
            self.dot = data[0].split(',')
            print(self.dot)
            idx1 = self.dot2index(int(self.dot[0]))
            idx2 = self.dot2index(int(self.dot[1]))
            print('%d.%d' % (idx1[0], idx1[1]), '%d.%d' % (idx2[0], idx2[1]))

            if self._textview is not None:
                if idx1 == idx2:
                    self._textview.mark_set('insert', '%d.%d' % (idx1[0], idx1[1]))
                else:
                    self._textview.tag_add('DOT', '%d.%d' % (idx1[0], idx1[1]), '%d.%d' % (idx2[0], idx2[1]))
        else:   # show file list at toplevel listbox
            listw = tk.Tk()
            listw.option_add('*font', ('FixedSys', 14))
            listb = tk.Listbox(listw)
            #sbar = tk.Scrollbar(listw, orient='v', command=listb.yview)
            #listb.configure(yscrollcommand=sbar.set)

            def getfilename(event):
                self.selectedfile = listb.get('active')
                print(self.selectedfile)
                listw.destroy()

            listb.bind('<Double-1>', getfilename) 
            listb.grid(column=0, row=0, sticky='nsew')
            #sbar.grid(column=1, row=0, sticky='ns')
            for f in data:
                listb.insert('end', f)
                
    def on_closing(self, event=None):
        ''' This function is to be called when the window is closed.'''
        if self.socket is not None:
            self.socket.close()
        root.quit()

    # skk related routine
    # add char to skkbuf
    def addCh(self, c):
        self.skkbuf += c

    # change skk mode by ESC key(Hira -> Kata -> Zenei -> Hira ...)
    def changeMode(self):
        if self.skk == 'Hira':
            self.skk = 'Kata'
        elif self.skk == 'Kata':
            self.skk = 'Zenei'
        elif self.skk == 'Zenei':
            self.skk = 'Hira'

    # show kanji koho
    def showKoho(self):
        if len(self.koho) != 0:
            self.delete('Henkan', 'insert')
            self.insert('insert', self.koho[self.kohop])
            self.kohop += 1
            if self.kohop > len(self.koho):
                self.kohop = 0
    
    # Ascii -> Kana -> Kanji
    def skkConvert(self):
        b = bytes(self.skkbuf, 'utf8')
        k = False
        for v in b:
            if v > 127: 
                k = True
                break
        if k:
            if self.skk == 'Hira' or self.skk == 'Kata':
                self.koho = self.getKoho(self.skkbuf)
        else:
            kana = self.getKana(self.skkbuf)

    # skk main routine
    # kana -> kanji: ^j
    # choose koho: Space
    # Kakutei: ^j or press any key
    def skkinput(self, event=None)
        key = '%s' % event.keysym

        if key == 'Escape':      # change mode
            self.changeMode()
        elif key == 'space':     # choose koho
            self.showKoho()
        elif key == 'Linefeed':  # ^j Ascii -> Hira, Kata -> Kanji
            self.skkConvert()
        else:
            self.addCh(key)

if __name__ == '__main__':

    root = tk.Tk()
    root.option_add('*font', ('FixSys', 14))
    
    msgw = tk.Entry(root)
    tview = scrolledtext.ScrolledText(root)
    cmdw = CommandWindow(root, msgwindow=msgw, textview=tview)
   
    cmdw.grid(column=0, row=0, sticky='nsew')
    msgw.grid(column=0, row=1, sticky='ew')
    tview.grid(column=0, row=2, sticky='nsew')
    
    root.protocol('WM_DELETE_WINDOW', cmdw.on_closing)
    root.mainloop()
