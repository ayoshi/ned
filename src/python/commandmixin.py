# -*- coding:utf-8 -*-
from socket import AF_INET, socket, SOCK_STREAM
from threading import Thread

class CommandMixin:
    '''
    Class to allow a Tkinter Text widget to handle ned command.

    To use this mixin, subclass from Tkinter.Text and the mixin, then write
    an __init__() method for the new class that calls _init().

    Then override the handleData() method to implement the behavior that
    you want to happen when received the output from ned server.
    '''

    def _init(self, **kwargs):
        '''Prepare ned server'''
        
        # TCP connection constant
        self.host = 'localhost'
        self.port = 1178

        # server socket
        self.socket = None
        
        # quit command to close all widget
        self.quit = 'q'
        
        # message window
        try:
            self._msgwindow = kwargs.pop('msgwindow')
        except KeyError:
            self._msgwindow = None

    def connectNed(self):
        s = socket(AF_INET, SOCK_STREAM)
        try:
            s.connect((self.host, self.port))
        except ConnectionRefusedError:
            self.insert('end', 'Ned sever not found!')
        else:
            if self._msgwindow is not None:
                self._msgwindow.insert('end', 'connecting ...')
            s.send(bytes('hi\n', 'utf8'))
            data = s.recv(1024).decode('utf8')
            if len(data) > 0:
                if self._msgwindow is not None:
                    self._msgwindow.insert('end', 'ok\n')
                self.socket = s

    def handleData(self, data):
        '''
        Handle received data, output to text viewer or display error
        Override this method in your class to do what you want.       
        '''
        pass

    def sendData(self, cmdstr):
        cmd = cmdstr + '\n'
        self.socket.send(bytes(cmd, 'utf8'))

    def recvData(self):      # thread
        '''This function is receiving data until window is closed.'''
        while True:
            try:
                data = self.socket.recv(1024).decode('utf8')
                self.handleData(data)
            except OSError:
                break
            if data == self.quit: break
 
