import strutils, sequtils, pegs
import parse, file, search, utf8utils as utf8    # unicode
import log

when defined(testing):
  const DEBUG = true
else:
  const DEBUG = false
  
const Module = "address"

proc matchFile*(r: string, a: var parse.Address, fl: FileList = file.filelist): bool
proc charAddr(pos: int, a: parse.Address, sign: int, msg: var string): parse.Address
proc lineAddr(lnum: int, a: parse.Address, sign: int, msg: var string): parse.Address
proc nextMatch*(r: string, a: var parse.Address, pos: int, sign: int): bool

# parse simple address
proc simpleAddr*(a: var parse.Address, fl: FileList = file.filelist): bool =
  var c: int
  var sign = 0
  var msg = ""
  result = true
  
  while true:
    discard parse.skipBl()
    c = parse.getc()

    if c notin Addrp or c in {ord(','), ord(';')}:
      parse.ungetc()
      break
    
    case c
    of ord('0')..ord('9'):        # line number
      when DEBUG: debugLog(Module, "line address")
      parse.ungetc()
      let ns = parse.getNum()
      when DEBUG: debugLog(Module, "line address ns => " & ns)
      a = lineAddr(parseInt(ns), a, sign, msg)
      if msg.len != 0:
        result = false
        echo msg
        break
      parse.ungetc()
    of ord('#'):         # char address
      when DEBUG: debugLog(Module, "char address")
      let ns = parse.getNum()
      #echo "char address getNum => ", ns
      a = charAddr(parseInt(ns), a, sign, msg)
      if msg.len != 0:
        result = false
        echo msg
        break
      parse.ungetc()
    of ord('.'):         # dot
      when DEBUG: debugLog(Module, "dot")
      a.r = a.f.dot
    of ord('$'):
      when DEBUG: debugLog(Module, "dallor")
      a.r.p1 = -1          # file.rfileSize(a.f)
      a.r.p2 = a.r.p1
    of ord('\''):        # mark
      when DEBUG: debugLog(Module, "mark")
      a.r = a.f.mark
    of ord('?'), ord('/'):         # sign
      when DEBUG: debugLog(Module, "regexp")
      var ret: bool
      var sp: int
      if c == ord('?'):
        sign = -sign
        if sign == 0: sign = -1
      # regexp
      let re = parse.getRegexp(c)
      if re.len == 0: return false
      else:
        sp = if sign >= 0: a.r.p2 else: a.r.p1
        ret = nextMatch(re, a, sp, sign)
        if not ret:
         if sign >= 0:     # and sp == file.fileSize(a.f):
           ret = nextMatch(re, a, 0, sign)
         if sign < 0:
           ret = nextMatch(re, a, a.r.p2, -sign)
        result = ret
    of ord('"'):         # file match
      when DEBUG: debugLog(Module, "file match")
      let re = parse.getRegexp(c)
      if matchFile(re, a):
        if a.f.unread:
          if not file.loadFile(a.f):
            break
          else: file.setCurFile(a.f)
      else:
        result = false
        echo "?file not match"
    of ord('+'), ord('-'):
      when DEBUG: debugLog(Module, "+ -")
      sign = 1
      if c == ord('-'): sign = -sign
    else:
      result = false
      echo "address?"
      break

# parse address string
proc parseAddr*(a: var parse.Address, fl: FileList = file.filelist): bool =
  var a1, a2: parse.Address
  var c: int
  var left = false

  a1 = a
  a2 = (r: (p1: -1, p2: 0), f: a.f)
  #echo "parseAddr a => ", repr(a)
  result = true
  
  while true:
    discard parse.skipBl()
    c = parse.getc()

    if c == -1 or c notin Addrp:
      parse.ungetc()
      break
    elif c notin {ord(','), ord(';')}:    # compound address
      parse.ungetc()
      if simpleAddr(a1):
        left = true
        if parse.nextc() == -1 or parse.nextc() notin Addrp:
          a = a1
        else: continue
      else: result = false
    else:
      when DEBUG: debugLog(Module, "compound address")
      if c == ord(','):
        if not left: a1.r.p1 = 0
        if parse.nextc() in {ord('+'), ord('-')}:
          a2.r.p1 = a1.r.p1
          #echo "parseAddr a2.r.p1 => ", a2.r.p1
        if simpleAddr(a2):
          if a2.r.p1 == -1:
           a.r.p1 = a1.r.p1
           a.r.p2 = -1
          else:
           a.r.p1 = a1.r.p1
           a.r.p2 = a2.r.p2
          if a.r.p2 != -1 and a.r.p1 > a.r.p2:
            echo "?address"
            result = false
        else: result = false
        when DEBUG: debugLog(Module, "parseAddr a => " & repr(a.r))
      else:
        a.f.dot = a.r

proc indexToOffset*(s: string, idx: int, f: var file.File): int =
  var offset, cnt = 0
  if f.cacheok:
    if f.lastindex.idx <= idx:
      offset = f.lastindex.offset
      cnt = f.lastindex.idx
  while offset < s.len:
    if cnt == idx: break
    offset += utf8.runeByteLen(s[offset])
    inc(cnt)
  #for c in utf8(s[offset..high(s)]):
  #  if cnt == idx: break
  #  offset += len(c)
  #  inc(cnt)
  if f.cacheok:
    f.lastindex.idx = idx
    f.lastindex.offset = offset
  return offset

proc offsetToIndex*(s: string, offset: int, f: var file.File): int =
  var i, cnt = 0
  if f.cacheok:
    if f.lastindex.offset <= offset:
      i = f.lastindex.offset
      cnt = f.lastindex.idx
  while i < s.len:
    if i == offset: break
    i += utf8.runeByteLen(s[i])
    inc(cnt)
  #for c in utf8(s[offset..high(s)]):
  #  if i == offset: break
  #  i += len(c)
  #  inc(cnt)
  if f.cacheok:
    f.lastindex.idx = cnt
    f.lastindex.offset = i
  return cnt
  
proc nextMatch*(r: string, a: var parse.Address, pos: int, sign: int): bool =
  when DEBUG: debugLog(Module, "pos => " & $pos & " sign => " & $sign)
  var ra: file.Range
  let s = file.snapshot(a.f)
  let sp = if pos == -1: file.rfileSize(a.f) - 1 else: pos

  result = true

  if len(a.f.sel) != 0:
    a.f.sel.delete(0, high(a.f.sel))

  let startp = indexToOffset(s, sp, a.f)
  let endp = file.fileSize(a.f) - 1
  if sign >= 0:
    if not search.execute(a.f, startp, s[startp..endp], r):
      result = false
  else:
    if not search.bexecute(a.f, s[0..startp], r):
      result = false

  if result:
    #a.f.lastpat = r
    ra = a.f.sel[0]
    a.r = (p1: offsetToIndex(s, ra.p1, a.f), p2: offsetToIndex(s, ra.p2, a.f) + 1)  # TODO 10/10
  else: echo "?search"

proc matchFile*(r: string, a: var parse.Address, fl: FileList = file.filelist): bool =
  let
    pat = peg(r)
  var file: file.File

  result = true
  for f in items(fl):
    if match(f.fstat.name, pat):
      file = f
      break
  if file != nil:
    a.f = file
  else:
    result = false

proc charAddr(pos: int, a: parse.Address, sign: int, msg: var string): parse.Address =
  var adr: parse.Address

  if sign == 0:
    adr.r.p1 = pos
    adr.r.p2 = adr.r.p1
    adr.f = a.f
  elif sign < 0:
    adr.r.p2 = a.r.p2 - pos
    adr.r.p1 = adr.r.p2
    adr.f = a.f
  elif sign > 0:
    adr.r.p2 = a.r.p2 + pos
    adr.r.p1 = adr.r.p2
    adr.f = a.f
  if adr.r.p1 < 0 or adr.r.p2 > file.rfileSize(a.f):
    msg = "?address range"
    adr = ((p1: -1, p2: -1), nil)
  return adr

proc lineAddr(lnum: int, a: parse.Address, sign: int, msg: var string): parse.Address =
  var adr: parse.Address
  var ln = lnum

  when DEBUG: debugLog(Module, "lnum => " & $ln & " sign => " & $sign)
  if sign >= 0:
    if sign == 0:
      adr.r = file.lineToRange(ln, a.f)
      adr.f = a.f
    else:
      let l1 = if a.r.p1 == 0: 0 else: file.offsetToLine(a.r.p1, a.f)
      #echo "lineAddr a.r.p1 => ", a.r.p1, " l1 => ", l1
      adr.r = file.lineToRange(l1 + ln, a.f)
      adr.f = a.f
  else:
    let l1 = file.offsetToLine(a.r.p1, a.f)
    adr.r = file.lineToRange(l1 - ln, a.f)
    adr.f = a.f
  if adr.r.p1 < 0 or adr.r.p2 > file.rfileSize(a.f):
    msg = "?address range"
    adr = ((p1: -1, p2: -1), nil)
  return adr

when isMainModule:
  block nextMatch:
    var f = file.initFile("COPYING")
    if file.loadFile(f):
      file.setCurFile(f)
    var r: file.Range
    var a: Address
    r = (p1: 0, p2: 0)
    a = (r: r, f: f)
    #echo "f => ", repr(f)
    if nextMatch("freedom", a, 0, 0):
      echo repr(f.sel)
    #echo "address => ", repr(a)

  block matchFIle:
    type
      TestFiles = array[0..3, string]
    var
      f: file.File
      files: TestFiles
      r: file.Range
      a: parse.Address
    files = ["foo", "piece.nim", "nimedit.nim", "test.txt"]
    for fn in items(files):
      f = initFile(fn)
      setFileList(f)

    #echo "file list => ", repr(file.filelist)
    if matchFile("piece.nim", a, file.filelist):
      echo "address => ", repr(a)
    else:
      echo "matchFile error"

  block simpleAddr:
    var f = file.initFile("COPYING")
    if file.loadFile(f): file.setCurFile(f)
    var r = (p1: -1, p2: 0)
    var a = (r: r, f: f)
    var msg: string
    msg = ""

    assert(charAddr(10, a, 0, msg).r == (p1: 10, p2: 10), "charAddr failed! '#10'")
    assert(lineAddr(3, a, 0, msg).r == (p1: 63, p2: 64), "lineAddr failed! 'l3'")

    parse.inputLine("#10+#20")
    assert(simpleAddr(a) == true, "simpleAddr failed! '#10+#20'")
    #echo "'#10+#20' address => ", repr(a.r)
    assert(a.r == (p1: 30, p2: 30), "address not correct! '#10+#20'")
    parse.inputLine("#30-#10")
    assert(simpleAddr(a) == true, "oarseAddr failed! '#30-#10'")
    assert(a.r == (p1: 20, p2: 20), "address not correct! '#30-#10'")
    parse.inputLine("100+100")
    assert(simpleAddr(a) == true, "simpleAddr failed! '100+100'")
    #echo "'100+100' address => ", repr(a.r)
    assert(a.r == (p1: 10620, p2: 10691), "address not correct! '100+100'")
    parse.inputLine("300-100")
    assert(simpleAddr(a) == true, "simpleAddr failed! '300-100'")
    assert(a.r == (p1: 10620, p2: 10691), "address not correct! '300-100'")
    # address reset"
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("/freedom/")
    assert(simpleAddr(a) == true, "simpleAddr failed! '/freedom/'")
    #echo "a.r => ", repr(a.r)
    assert(a.r == (p1: 385, p2: 392), "address not correct! '/freedom/'")
    # nextMatch again
    parse.inputLine("/freedom/")
    assert(simpleAddr(a) == true, "simpleAddr failed! '/freedom/'")
    assert(a.r == (p1: 492, p2: 499), "address not correct! '/freedom/'")
    parse.inputLine("?freedom?")
    assert(simpleAddr(a) == true, "simpleAddr failed! '?freedom?'")
    assert(a.r == (p1: 385, p2: 392), "address not correct! '?freedom?'")

    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("+/freedom/+/freedom/")
    assert(simpleAddr(a) == true, "simpleAddr failed! '+/freedom/+/freedom/'")
    assert(a.r == (p1: 492, p2: 499), "address not correct! '+/freedom/+/freedom/'")
    parse.inputLine("0+/freedom/")
    assert(simpleAddr(a) == true, "simpleAddr failed! '0+/freedom/'")
    #echo "'0+/freedom/' address => ", repr(a.r)
    assert(a.r == (p1: 385, p2: 392), "address not correct! '0+/freedom/'")
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("$-/freedom/")
    assert(simpleAddr(a) == true, "simpleAddr failed! '$-/freedom/'")
    #echo "'$-/freedom/' address => ", repr(a.r)
    assert(a.r == (p1: 1033, p2: 1040), "address not correct! '$-/freedom/'")
    echo "\n** simpleAddr all test OK **\n"
    
  block parseAddr:
    var f = file.initFile("COPYING")
    if file.loadFile(f): file.setCurFile(f)
    var r = (p1: -1, p2: 0)
    var a = (r: r, f: f)
    var msg: string
    msg = ""

    assert(charAddr(10, a, 0, msg).r == (p1: 10, p2: 10), "charAddr failed! '#10'")
    assert(lineAddr(3, a, 0, msg).r == (p1: 63, p2: 64), "lineAddr failed! 'l3'")

    parse.inputLine("#10+#20")
    assert(parseAddr(a) == true, "parseAddr failed! '#10+#20'")
    #echo "'#10+#20' address => ", repr(a.r)
    assert(a.r == (p1: 30, p2: 30), "address not correct! '#10+#20'")
    parse.inputLine("#30-#10")
    assert(parseAddr(a) == true, "oarseAddr failed! '#30-#10'")
    assert(a.r == (p1: 20, p2: 20), "address not correct! '#30-#10'")
    parse.inputLine("100+100")
    assert(parseAddr(a) == true, "parseAddr failed! '100+100'")
    #echo "'100+100' address => ", repr(a.r)
    assert(a.r == (p1: 10620, p2: 10691), "address not correct! '100+100'")
    parse.inputLine("300-100")
    assert(parseAddr(a) == true, "parseAddr failed! '300-100'")
    assert(a.r == (p1: 10620, p2: 10691), "address not correct! '300-100'")
    # address reset"
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '/freedom/'")
    #echo "a.r => ", repr(a.r)
    assert(a.r == (p1: 385, p2: 392), "address not correct! '/freedom/'")
    # nextMatch again
    parse.inputLine("/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '/freedom/'")
    echo "a.r => ", repr(a.r)
    assert(a.r == (p1: 492, p2: 499), "address not correct! '/freedom/'")
    parse.inputLine("?freedom?")
    assert(parseAddr(a) == true, "parseAddr failed! '?freedom?'")
    assert(a.r == (p1: 385, p2: 392), "address not correct! '?freedom?'")

    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("+/freedom/+/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '+/freedom/+/freedom/'")
    assert(a.r == (p1: 492, p2: 499), "address not correct! '+/freedom/+/freedom/'")
    parse.inputLine("0+/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '0+/freedom/'")
    #echo "'0+/freedom/' address => ", repr(a.r)
    assert(a.r == (p1: 385, p2: 392), "address not correct! '0+/freedom/'")
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("$-/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '$-/freedom/'")
    #echo "'$-/freedom/' address => ", repr(a.r)
    assert(a.r == (p1: 1033, p2: 1040), "address not correct! '$-/freedom/'")
 
    parse.inputLine("#100,#300")
    assert(parseAddr(a) == true, "parseAddr failed! '#100,#300'")
    parse.inputLine(",#300")
    assert(parseAddr(a) == true, "parseAddr failed! ',#300'")
    echo "',#300' address => ", repr(a.r)
    assert(a.r == (p1: 0, p2: 300), "address not correct! ',#300'")
    parse.inputLine("1,200")
    assert(parseAddr(a) == true, "parseAddr failed! '1,500'")
    #echo "'1,500' address => ", repr(a.r)
    assert(a.r == (p1: 0, p2: 10691), "address not correct! '1,500'")
    parse.inputLine(",200")
    assert(parseAddr(a) == true, "parseAddr failed! ',200'")
    assert(a.r == (p1: 0, p2: 10691), "address not correct! ',200'")
    #address reset
    r = (p1: 0, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("/WARRANTY/,/CORRECTION/")
    assert(parseAddr(a) == true, "parseAddr failed! '/WARRANTY/,/CORRECTION/'")
    #echo "'/WARRANTY/,/CORRECTION/' address => ", repr(a.r)
    assert(a.r == (p1: 13856, p2: 14467), "address not correct! '/WARRANTY/,/CORRECTION/'")
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine(",")
    assert(parseAddr(a) == true, "parseAddr failed! ','")
    #echo "',' address => ", repr(a.r)
    assert(a.r == (p1: 0, p2: -1), "address not correct! ','")     # p2: 17987
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine(",$")
    assert(parseAddr(a) == true, "parseAddr failed! ',$'")
    echo "',$' address => ", repr(a.r)
    assert(a.r == (p1: 0, p2: -1), "address not correct! ',$'")
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("1,$")
    assert(parseAddr(a) == true, "parseAddr failed! '1,$'")
    assert(a.r == (p1: 0, p2: -1), "address not correct! '1,$'")

    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine("0/freedom/,$-/freedom/")
    assert(parseAddr(a) == true, "parseAddr failed! '0/freedom/,$-/freedom/'")
    #echo "'0/freedom/,$-/freedom/' address => ", repr(a.r)
    assert(a.r == (p1: 385, p2: 1040), "address not correct! '0/freedom/,$-/freedom/'")
    r = (p1: -1, p2: 0)
    a = (r: r, f: f)
    parse.inputLine(",+10")
    assert(parseAddr(a) == true, "parseAddr failed! ',+10'")
    echo "',+10' address => ", repr(a.r)
    assert(a.r == (p1: 0, p2: 321), "address not correct! ',+10'")
    parse.inputLine("50,+10")
    assert(parseAddr(a) == true, "parseAddr failed! '50,+10'")
    echo "'50,+10' address => ", repr(a.r)
    assert(a.r == (p1: 2409, p2: 2943), "address not correct! '50,+10'")
    echo "all test OK"
