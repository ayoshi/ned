import strutils, tables, sequtils, os, osproc, unicode
import parse, address, file, search, unix, server         #, nfmain, nforth
import log
#import nimprof

when defined(testing):
  const DEBUG = true
else:
  const DEBUG = false
  
const Module = "command"

const
  Linex = {ord('\n')}
  Wordx = parse.BL + Linex
  PLAN9DIR = "/usr/local/plan9"

proc nlCmd(cmd: parse.Cmd): bool   # DONE
proc aCmd(cmd: parse.Cmd): bool    # DONE
proc bCmd(cmd: parse.Cmd): bool    # DONE
proc cCmd(cmd: parse.Cmd): bool    # DONE
proc dCmd(cmd: parse.Cmd): bool    # DONE
proc udCmd(cmd: parse.Cmd): bool   # DONE
proc eCmd(cmd: parse.Cmd): bool    # DONE
proc fCmd(cmd: parse.Cmd): bool    # DONE
proc gCmd(cmd: parse.Cmd): bool    # DONE
proc iCmd(cmd: parse.Cmd): bool    # DONE
proc kCmd(cmd: parse.Cmd): bool    # DONE
proc mCmd(cmd: parse.Cmd): bool    # DONE
proc nCmd(cmd: parse.Cmd): bool    # DONE
proc pCmd(cmd: parse.Cmd): bool    # DONE
proc qCmd(cmd: parse.Cmd): bool    # DONE
proc sCmd(cmd: parse.Cmd): bool    # DONE
proc uCmd(cmd: parse.Cmd): bool    # DONE
proc wCmd(cmd: parse.Cmd): bool    # DONE
proc xCmd(cmd: parse.Cmd): bool    # DONE
proc uxCmd(cmd: parse.Cmd): bool   # DONE
proc shellCmd(cmd: parse.Cmd): bool    # TODO use pipe or fifo
proc eqCmd(cmd: parse.Cmd): bool   # DONE
proc cdCmd(cmd: parse.Cmd): bool   # DONE

# nforth

#proc zCmd(cmd: parse.Cmd): bool

proc execCmd*(cmd: var parse.Cmd): bool
proc addCmdHistory(cmd: parse.Cmd, odot: file.Range, count: int)

type
  ArgTyp* = enum Txt, Re, Adr
  AdrTyp* = enum aDot, aAll
  CmdMem* = tuple[cmd: char, fname: string, dot: file.Range, count: int]

##    sam command table
##    cmdc  defarg  defcmd  defadr  count   token   fn
##    '\n'    No      '0'    aDot     0     None   nlCmd
##    'a'     Txt     '0'    aDot     0     None   aCmd
##    'b'     No      '0'    aNo      0     Linex  bCmd
##    'B'     No      '0'    aNo      0     Linex  bCmd
##    'c'     Txt     '0'    aDot     0     None   cCmd
##    'd'     No      '0'    aDot     0     None   dCmd
##    'D'     No      '0'    aNo      0     Linex  udCmd
##    'e'     No      '0'    aNo      0     Wordx  eCmd
##    'f'     No      '0'    aNo      0     Wordx  fCmd
##    'g'     Re      'p'    aDot     0     None   gCmd
##    'i'     Txt     '0'    aDot     0     None   iCmd
##    'k'     No      '0'    aDot     0     None   kCmd
##    'm'     Adr     '0'    aDot     0     None   mCmd
##    'n'     No      '0'    aNo      0     None   nCmd
##    'p'     No      '0'    aDot     0     None   pCmd
##    'q'     No      '0'    aNo      0     None   qCmd
##    'r'     No      '0'    aDot     0     Wordx  eCmd
##    's'     Re      '0'    aDot     1     None   sCmd
##    't'     Adr     '0'    aDot     0     None   mCmd
##    'u'     No      '0'    aNo      2     None   uCmd
##    'v'     Re      'p'    aDot     0     None   gCmd
##    'w'     No      '0'    aAll     0     Wordx  wCmd
##    'x'     Re      'p'    aDot     0     None   xCmd
##    'y'     Re      'p'    aDot     0     None   xCmd
##    'X'     Re      'f'    aNo      0     None   uxCmd
##    'Y'     Re      'f'    aNo      0     None   uxCmd
##    '!'     No      '0'    aNo      0     Linex  shellCmd
##    '>'     No      '0'    aDot     0     Linex  shellCmd
##    '<'     No      '0'    aDot     0     Linex  shellCmd
##    '|'     No      '0'    aDot     0     Linex  shellCmd
##    '='     No      '0'    aDot     0     Linex  eqCmd
##    'C'     No      '0'    aNo      0     Wordx  cdCmd

#    'z'     Txt     '0'    aDot     0     None   zCmd
  
let
  defarg = {'a': Txt, 'c': Txt, 'g': Re, 'i': Txt, 'm': Adr, 's': Re, 't': Adr, 'v': Re,
            'x': Re, 'y': Re, 'X': Re, 'Y': Re, 'z': Txt}.totable
  defcmd = {'g': 'p', 'v': 'p', 'x': 'p', 'y': 'p', 'X': 'f', 'Y': 'f'}.totable
  defadr = {'\n': aDot, 'a': aDot, 'c': aDot, 'd': aDot, 'g': aDot, 'i': aDot, 'k': aDot,
            'm': aDot, 'p': aDot, 'r': aDot, 's': aDot, 't': aDot, 'v': aDot, 'w': aAll,
            'x': aDot, 'y': aDot, '>': aDot, '<': aDot, '|': aDot, '=': aDot, 'z': aDot}.totable
  count = {'s': 1, 'u': 2}.totable
  token = {'b': Linex, 'B': Linex, 'D': Linex, 'e': Wordx, 'f': Wordx, 'r': Wordx, 'w': Wordx,
           '!': Linex, '>': Linex, '<': Linex, '|': Linex, '=': Linex, 'C': Wordx}.totable
  fn = {'\n': nlCmd, 'a': aCmd, 'b': bCmd, 'B': bCmd, 'c': cCmd, 'd': dCmd, 'D': udCmd,
        'e': eCmd, 'f': fCmd, 'g': gCmd, 'i': iCmd, 'k': kCmd, 'm': mCmd, 'n': nCmd,
        'p': pCmd, 'q': qCmd, 'r': eCmd, 's': sCmd, 't': mCmd, 'u': uCmd, 'v': gCmd,
        'w': wCmd, 'x': xCmd, 'y': xCmd, 'X': uxCmd, 'Y': uxCmd, '!': shellCmd, '>': shellCmd,
        '<': shellCmd, '|': shellCmd, '=': eqCmd, 'C': cdCmd}.totable       #, 'z': zCmd}.totable
  undoable = {'a', 'c', 'i', 'd', 's', 'm', 't', 'r', 'f', '<', '|', '{', 'z'}

var nest = 0         # for loop, group command { }
var quitok* = false   # qCmd
var cmdhistory = newSeq[CmdMem]()
var cmdhistptr = 0
var cmdundoptr = 0
var curClient*: Socket

proc parseCmd*(skk: bool = false): Cmd =
  var ccmd, ncmd: parse.Cmd
  var c: int
  var a: parse.Address
  var r: file.Range
  var f = file.curfile      # all command apply to the curfile!
  var fl = file.filelist
  var sign = false

  new(ccmd)
  ccmd.sadr.r = (p1: -1, p2: 0)
  c = parse.skipBl()   # skip spaces
  if c == -1 or c > 127:
    return nil
  if c in parse.Addrp:
    when DEBUG: debugLog(Module, "enter address c => " & chr(c))
    r = f.dot
    a = (r: r, f: f)
    if not address.parseAddr(a, fl):
      return nil
    if a.f != nil and a.f.unread:
      if file.loadFile(a.f): file.setCurFile(a.f)
      else: return nil
    ccmd.sadr = a

  if ccmd.sadr.f == nil: ccmd.sadr.f = f
  discard parse.skipBl()
  c = parse.getc()
  when DEBUG: debugLog(Module, "First c => " & chr(c))
  ccmd.cmd = chr(c)
  #when DEBUG: debugLog(Module, "nextc => " & chr(parse.nextc()) & " ccmd.cmd => " & ccmd.cmd)
  if ccmd.cmd == 'c' and parse.nextc() == ord('d'):
    ccmd.cmd = 'C'
    discard parse.getc()
  if haskey(fn, ccmd.cmd):
    when DEBUG: debugLog(Module, "Enter normal command")
    #ccmd.sadr.f = f       # set curfile for all command
    if ccmd.cmd == '\n':
      return ccmd
    if haskey(count, ccmd.cmd):
      when DEBUG: debugLog(Module, "Enter count")
      c = parse.nextc()
      if c == ord('-'):
        sign = true
        discard parse.getc()
        c = parse.nextc()
      if c in parse.Num:
        ccmd.args = if sign: '-' & parse.getNum() else: parse.getNum()
        when DEBUG: debugLog(Module, "getNum => " & ccmd.args)
        parse.ungetc()
    if haskey(defarg, ccmd.cmd):
      when DEBUG: debugLog(Module, "Enter has defarg")
      case defarg[ccmd.cmd]
      of Re:
        # x without pattern -> .*\n, indicated ccmd.args = ""
        # X without pattern is all files
        when DEBUG: debugLog(Module, "defarg Re")
        if (ccmd.cmd != 'x' and ccmd.cmd != 'X') or
           (parse.nextc() notin {ord(' '), ord('\t'), ord('\n')}):
          when DEBUG: debugLog(Module, "defarg Re: c => " & chr(c))
          c = parse.getc()
          if c == ord('\n') or c == -1:
            echo "No pattern"           # Enopattern
            return nil
          when DEBUG: debugLog(Module, "defarg Re: getc => " & chr(c))
          if okDelim(c):
            if len(ccmd.args) != 0 and isDigit(ccmd.args):  # has count args
              ccmd.args = chr(c) & ccmd.args & chr(c) & parse.getRegexp(c)
            else:
              ccmd.args = chr(c) & parse.getRegexp(c)
            when DEBUG: debugLog(Module, "ccmd.args => " & ccmd.args)
            if ccmd.cmd == 's':        # regexp/replace
              ccmd.args = ccmd.args & chr(c) & parse.getRegexp(c, raw = true)
              if  parse.nextc() == ord('g'): ccmd.flag = chr(parse.getc())
          else:
            echo "Bad delimitter: ", chr(c)
            return nil
        if haskey(defcmd, ccmd.cmd):
          c = parse.skipBl()
          if c == ord('\n'):
            ccmd.flag = defcmd[ccmd.cmd]    # p, f
          else:
            ccmd.next = parseCmd()
            if ccmd.next == nil:
              echo "panic defcmd"
              return nil
      of Adr:                  # m, t
        when DEBUG: debugLog(Module, "defarg Adr")
        discard parse.skipBl()
        ccmd.dadr.f = file.curfile       # need it! TODO?
        if not address.parseAddr(ccmd.dadr, fl):
          echo "?address"         # Eaddress
          return nil
      of Txt:
        when DEBUG: debugLog(Module, "defarg txt")
        c = parse.getc()
        when DEBUG: debugLog(Module, "c => " & chr(c))
        if c == ord('\n'):
          if ccmd.cmd == 'z':
            ccmd.args = ""
          else:
            ccmd.args = if skk: parse.getInput(true) else: parse.getInput()
        else:
          if parse.okDelim(c):
            when DEBUG: debugLog(Module, "getRegexp")
            ccmd.args = parse.getRegexp(c, raw = true)
            when DEBUG: debugLog(Module, "ccmd.args => " & ccmd.args)
    elif haskey(token, ccmd.cmd):
      when DEBUG: debugLog(Module, "token")
      # take until nl
      discard parse.skipBl()
      c = parse.nextc()
      when DEBUG: debugLog(Module, "token nextc => " & chr(c))
      if c != ord('\n'):
        if c == ord('<'):    # shell command
          discard parse.getc()
          let args = parse.getText(ord('\n'))
          let path = if existsDir(PLAN9DIR): PLAN9DIR & "/bin/" else: ""
          let cmdpath = if len(path) != 0: path & args else: args
          let (outp, errC) = execCmdEx(cmdpath)
          if errC == 0: ccmd.args = outp
        else: ccmd.args = parse.getText(ord('\n'))
    else:    # atnl
      discard parse.skipBl()
      #discard parse.getc()
      if parse.getc() != ord('\n'):
        echo "no newline found"       # Enewline
  else:           # no in fn table
    case ccmd.cmd
    of '{':
      var cmd: Cmd
      while true:
        if parse.skipBl() == ord('\n'):
          discard parse.getc()
        ncmd = parseCmd()
        if ncmd == nil: break
        if nest == 0:
            ccmd.next = ncmd
            #ccmd.next.sadr = ccmd.sadr
        else:
            cmd.next = ncmd
            #cmd.next.sadr = ccmd.sadr
        inc(nest)
        cmd = ncmd
    of '}':
      if nest == 0:
        echo "no left brace"    # Enolbrace
      else: nest = 0
      return nil
    else:
      echo "Error parseCmd"
      return nil
  return ccmd

# execute command
proc execCmd*(cmd: var parse.Cmd): bool =
  var cp: parse.Cmd
  var f = cmd.sadr.f
  let odot = cmd.sadr.r
  
  result = true
  if haskey(defadr, cmd.cmd):
    if defadr[cmd.cmd] == aDot:   # and cmd.sadr.r == (p1: 0, p2: 0):
      let r = cmd.sadr.r
      if r.p1 != -1: f.dot = cmd.sadr.r
    if defadr[cmd.cmd] == aAll:
      cmd.sadr.r.p1 = 0
      cmd.sadr.r.p2 = file.rfileSize(f)
  if cmd.cmd == 'f': cmd.sadr.f = file.curfile
  if cmd.cmd == '{':
    cp = cmd
    while cp.next != nil:
      cp = cp.next
      when DEBUG: debugLog(Module, "execCmd group next.cmd => " & cp.cmd)
      if not execCmd(cp):
        result = false
        break
  else:
    result = fn[cmd.cmd](cmd)
  if result:
    if cmd.cmd in undoable and nest == 0 and cmd.flag != 'g':    # not loop, group command
        addCmdHistory(cmd, odot, 1)

include xec

# newline command
proc nlCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call newline command => " & repr(cmd))
  var f = cmd.sadr.f
  let r = cmd.sadr.r
  let lst = file.lastLine(f)
  if r.p1 == -1:  # f.dot == f.ndot
    let l = file.offsetToLine(f.dot.p2, f)
    if l < lst:
      let r2 = file.lineToRange(l, f)
      f.ndot = r2
      result = display(r2, f)
    else: result = false
  else:
    result = display(r, f)

# append command
proc aCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call append command => " & repr(cmd))
  var f = cmd.sadr.f
  var r = cmd.sadr.r
  if r.p1 + r.p2 == -1: r = f.dot
  var s: string
  var rp2: int
  #if f.fstat.size != 0:
  s = file.snapshot(f)
  rp2 = if r.p2 == -1: s.len else: address.indexToOffset(s, r.p2, f)
  #else: rp2 = 0       # new file
  file.appendStr(rp2, cmd.args, f)
  file.setDot(r.p1, r.p2 + runeLen(cmd.args), f)
  result = true

# b command
proc bCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call b command => " & repr(cmd))
  var f, fobj: file.File
  let files = splitWhitespace(cmd.args)

  result = true
  if cmd.cmd == 'b':
    fobj = file.getFileObj(files[0])
  else:   # 'B' cmommand
    for fn in items(files):
      f = file.getFileObj(fn);
      if f == nil:
        f = file.initFile(fn)
        file.setFileList(f)
    fobj = getFileObj(files[high(files)])
  if fobj != nil and fobj.unread:
    if file.loadFile(fobj):
      file.setCurFile(fobj)
    else:
      echo "can't load file: ", fobj.fstat.name
      result = false

# change command
proc cCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call change command" & repr(cmd))
  var f = cmd.sadr.f
  let r = if cmd.sadr.r.p1 == -1: f.dot else: cmd.sadr.r
  let s = file.snapshot(f)
  let rp1 = address.indexToOffset(s, r.p1, f)
  let rp2 = if r.p2 == -1: s.len else: address.indexToOffset(s, r.p2, f)
  file.replaceStr(rp1, rp2 - rp1, cmd.args, f)
  file.setDot(r.p1, r.p1 + runeLen(cmd.args), f)
  result = true

# delete command
proc dCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call delete command => " & repr(cmd))
  var f = cmd.sadr.f
  let r = if cmd.sadr.r.p1 == -1: f.dot else: cmd.sadr.r
  let s = file.snapshot(f)
  let rp1 = address.indexToOffset(s, r.p1, f)
  let rp2 = if r.p2 == -1: s.len else: address.indexToOffset(s, r.p2, f)
  file.deleteStr(rp1, rp2 - rp1, f)
  if r.p1 == 0 and s.len == 0:       # file.fileSize(f) == 0:
    file.setDot(0, 0, f)
  else:
    file.setDot(r.p1 + 1, r.p1 + 1, f)
  result = true

# D command
proc udCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call D command => " & repr(cmd))
  if cmd.args == "":
    if not file.curfile.deleted and okDelete(file.curfile):
      file.curfile.deleted = true
  else:
    for fn in splitWhitespace(cmd.args):
      var i = 0
      for f in items(file.filelist):
        if f.fstat.name == fn:
          if not f.deleted and okDelete(file.filelist[i]):
            f.deleted = true
        inc(i)
  result = true

# edit command
proc eCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call edit command => " & repr(cmd))
  var f = cmd.sadr.f
  let r = cmd.sadr.r

  result = true
  if f == nil: result = false
  elif cmd.cmd != 'r':
    file.deleteFileFromList(f.fstat.name)
    var fobj = file.initFile(cmd.args)
    if file.loadFile(fobj):
      file.setFileList(fobj)
      file.setCurFile(fobj)
    else: result = false
  else:     # 'r'
    if existsFile(cmd.args):
      let str = readFile(cmd.args)
      #echo "eCmd cmd = r r.p1 => ", r.p1, " r.p2 - r.p1 => ", r.p2 - r.p1
      if r.p1 == -1 and r.p2 == 0:
        file.appendStr(0, str, f)
      else:
        let s = file.snapshot(f)
        let rp1 = address.indexToOffset(s, r.p1, f)
        let rp2 = if r.p2 == -1: s.len else: address.indexToOffset(s, r.p2, f)
        file.replaceStr(rp1, rp2 - rp1, str, f)
      file.setDot(r.p2, r.p2 + runeLen(str), f)
    else: result = false

# file command
proc fCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call file command => " & repr(cmd))
  var f = cmd.sadr.f
  if len(cmd.args) != 0: file.setFileName(cmd.args, f)
  echo fileState(f) & ' ' & f.fstat.name
  result = true

# global command
#proc gCmd(cmd: parse.Cmd): bool =
#  #when DEBUG: debugLog(Module, "call global command => " & repr(cmd))
#  var f = cmd.sadr.f
#  var a = cmd.sadr
#  var p1, p2: int
#  let s = file.snapshot(f)
#
#  result = true
#  if a.r.p2 == -1:
#    a.r.p2 = file.rfileSize(f)
#    p1 = 0
#    p2 = s.len - 1
#  else:
#    p1 = address.indexToOffset(s, a.r.p1, f)
#    p2 = if a.r.p2 == file.rfileSize(f): s.len - 1
#         else: address.indexToOffset(s, a.r.p2, f)
#  if search.execute(f, p1, s[p1..p2], cmd.args) xor cmd.cmd == 'v':
#    if cmd.next != nil:
#      cmd.next.sadr.f = f
#      cmd.next.sadr.r = (a.r.p1, a.r.p2)
#      file.setDot(a.r.p1, a.r.p2, f)
#      result = execCmd(cmd.next)
#    if cmd.flag == 'p':
#      result = display((a.r.p1, a.r.p2), f)
#  else:
#    when DEBUG: debugLog(Module, "no match global")
#    result = false

proc gCmd(cmd: parse.Cmd): bool =
  #when DEBUG: debugLog(Module, "call global command => " & repr(cmd))
  var f = cmd.sadr.f
  var r = cmd.sadr.r
  var p1, p2: int
  let s = file.snapshot(f)
  let rsize = file.rfileSize(f)

  if r.p2 == -1:
    r.p2 = rsize
    p1 = 0
    p2 = s.len - 1
  else:
    p1 = address.indexToOffset(s, r.p1, f)
    p2 = if r.p2 == rsize: s.len - 1
         else: address.indexToOffset(s, r.p2, f)
  let re = cmd.args[1..high(cmd.args)]
  if search.execute(f, p1, s[p1..p2], re) xor cmd.cmd == 'v':
    if cmd.next != nil:
      cmd.next.sadr.f = f
      cmd.next.sadr.r = (r.p1, r.p2)
      file.setDot(r.p1, r.p2, f)
      result = execCmd(cmd.next)
    if cmd.flag == 'p':
      result = display((r.p1, r.p2), cmd.sadr.f)
  else:
    when DEBUG: debugLog(Module, "no match global")
    result = false

# insert command
proc iCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call insert command => " & repr(cmd))
  var f = cmd.sadr.f
  let r = if cmd.sadr.r.p1 == -1: f.dot else: cmd.sadr.r
  let s = file.snapshot(f)
  let rp1 = address.indexToOffset(s, r.p1, f)
  file.appendStr(rp1, cmd.args, f)
  file.setDot(r.p1, r.p1 + runeLen(cmd.args), f)
  result = true

# k command
proc kCmd(cmd: parse.Cmd): bool =
  var f = cmd.sadr.f
  if f == nil:
    echo "no file"
    result = false
  else:
    f.mark = cmd.sadr.r
    when DEBUG: debugLog(Module, "call k command => " & repr(cmd))
    result = true

# move command
proc mCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call move command => " & repr(cmd))
  if cmd.cmd == 'm':
    move(cmd.sadr, cmd.dadr)
  else:
    copy(cmd.sadr, cmd.dadr)
  result = true

# file list command
proc nCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call file list command => " & repr(cmd))
  file.showFileList()
  result = true

# print cmd
proc pCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call print command => " & repr(cmd))
  var f = cmd.sadr.f
  let r = if cmd.sadr.r.p1 == -1:
            f.dot
          elif cmd.sadr.r.p2 == -1:
            (p1: 0, p2: file.rfileSize(f))
          else: cmd.sadr.r
  result = display(r, f)

# quit command
proc qCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call quit command => " & repr(cmd))
  if quitok: result = true
  else:
    var flag = true
    var i = 0
    for f in items(file.filelist):
      if file.okDelete(file.filelist[i]):
        if not f.unread:
          closeFile(file.filelist[i])
      else:
        #echo "?changed"
        flag = false
      inc(i)
    result = flag
    quitok = true

# substitute command
proc sCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call substitute command => " & repr(cmd))
  var r = cmd.sadr.r
  var f = cmd.sadr.f
  var p1, p2: int
  let odot = f.dot
  result = true

  if r == (-1, 0): r = f.dot

  let delim = cmd.args[0]
  let args = split(cmd.args[1..high(cmd.args)], delim)
  if len(args) > 3 or len(args) <= 1:
    echo "wrong arguments number"
    result = false
  elif len(args) == 3 and not isDigit(args[0]):
    echo "wrong arguments: count => ", args[0], " is not number"
    result = false
  elif isDigit(args[0]) and cmd.flag == 'g':
    echo "wrong arguments: count => ", args[0], " and 'g' flag"
    result = false
  elif r.p1 == r.p2 or f == nil:
    echo "address not set or no file"
    result = false
  else:
    var n = 0
    var re, by: string
    if len(args) == 3 and cmd.flag != 'g':
      n = parseInt(args[0]) - 1
      re = args[1]
      by = args[2]
    else:
      re = args[0]
      by = args[1]
    # replace ampersant
    if contains(by, '&'): by = replaceAmpersant(by, re)

    let s = file.snapshot(f)
    let rl = runeLen(s)
    let rp1 = address.indexToOffset(s, r.p1, f)
    let rp2 = if r.p2 == -1 or r.p2 == file.rfileSize(f): s.len - 1
              else: address.indexToOffset(s, r.p2, f)
    if search.execute(f, rp1, s[rp1..rp2], re):
      if cmd.flag != 'g':
        if n < f.sel.len:
          (p1, p2) = f.sel[n]
          file.replaceStr(p1, p2 - p1 + 1, by, f)
          let (dp1, dp2 ) = f.dot
          let ln = file.offsetToLine(dp1, f)
          let (np1, np2) = file.lineToRange(ln, f)
          file.setDot(np1, np2, f)
        else:
          echo "?substitution"
          result = false
      else:
        var offset = 0
        for r in items(f.sel):
          file.replaceStr(offset + r.p1, r.p2 - r.p1 + 1, by, f)
          offset += by.len - (r.p2 - r.p1 + 1)
          #echo "sCmd g r.p1 => ", offset + r.p1, " by.len => ", by.len
        let diff = runeLen(file.snapshot(f)) - rl
        file.setDot(r.p1, r.p2 + diff, f)
        addCmdHistory(cmd, odot, len(f.sel))
    else:
      result = false

# undo command
proc uCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call undo command => " & repr(cmd))
  var f: file.File
  var i = if cmd.args == "": 1 else: parseInt($cmd.args)
  var sign = if i >= 0: 1 else: -1
  var cc: char
  var fn: string
  var dt: file.Range
  var cnt: int

  result = true
  if cmdhistptr == 0: result = false
  elif cmdundoptr == 0 and i >= 0: result = false
  elif cmdhistptr == cmdundoptr and i < 0: result = false
  else:
    if sign == 1:
      while i > 0:
        if cmdundoptr > 0:
          dec(cmdundoptr)
          (cc, fn, dt, cnt) = cmdhistory[cmdundoptr]
          f = file.getFileObj(fn)
          file.undoFile(cc, cnt, f)
          if f.dot != dt: f.dot = dt
          if cc == 'f':   # save fname for redo
            cmdhistory[cmdundoptr] = (cmd: cc,
                                      fname: f.fstat.name,
                                      dot: dt,
                                      count: cnt)
          dec(i)
    else:
      while i < 0:
        if cmdundoptr <= cmdhistptr:
          (cc, fn, dt, cnt) = cmdhistory[cmdundoptr]
          f = file.getFileObj(fn)
          file.undoFile(cc, cnt * sign, f)
          if f.dot != dt: f.dot = dt
          if cc == 'f':   # save fname for undo
            cmdhistory[cmdundoptr] = (cmd: cc,
                                      fname: f.fstat.name,
                                      dot: dt,
                                      count: cnt)
          inc(cmdundoptr)
          inc(i)

# write command
proc wCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call write command => " & repr(cmd))
  var name, path: string
  var f = cmd.sadr.f
  var wsize: int
  if len(cmd.args) != 0:
    path = cmd.args
    if path[0] == '~':
      path = unix.tilde(path)
    if not unix.okDir(path):
      echo "dir not exist: ", path
      return false
  else:
    path = f.fstat.path
  name = f.fstat.name
  if name == "noname":
    echo "file name not set"
    return false
  if (cmd.sadr.r.p1 == 0 and cmd.sadr.r.p2 == 0) or
     (cmd.sadr.r.p1 == 0 and cmd.sadr.r.p2 == file.rfileSize(f)):
    wsize = file.saveFile(path, f)
  else:
    let s = file.snapshot(f)
    let rp1 = address.indexToOffset(s, cmd.sadr.r.p1, f)
    let rp2 = address.indexToOffset(s, cmd.sadr.r.p2, f)
    wsize = file.saveFile(path, f, rp1, rp2)
  file.setFileTogleClean(f)
  if f.fstat.size != 0: echo name & ": #", wsize
  else: echo name & ": (new file) #", wsize
  result = true

# x loop command
proc xCmd(cmd: parse.Cmd): bool =
  cmd.sadr.f = file.curfile
#  when DEBUG: debugLog(Module, "call x loop command => " & repr(cmd))
  if len(cmd.args) != 0:
    result = looper(cmd)
  else: result = linelooper(cmd)

# X loop command
proc uxCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call X file loop command => " & repr(cmd))
  var f, fobj: file.File
  var flist = newSeq[file.File]()

  result = true
  var i = 0
  while i < len(file.filelist):
    f = file.filelist[i]
    if len(cmd.args) != 0:
      if filematch(cmd.args, f):
        if cmd.cmd == 'X':
          fobj = f
        else:
          inc(i)
          continue
      else:
        if cmd.cmd == 'Y':
          fobj = f
        else:
          inc(i)
          continue
    else: fobj = f
    if fobj.unread and not file.loadFile(fobj):
      echo "can't load: ", fobj.fstat.name
      inc(i)
      continue
    setCurFile(fobj)
    flist.add(fobj)
    if cmd.next != nil:
      cmd.next.sadr.f = fobj
      result = execCmd(cmd.next)
    inc(i)
  if cmd.flag == 'f':
    for f in items(flist):
      echo file.fileState(f) & ' ' & f.fstat.name

# shell command
proc shellCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call shell command => " & repr(cmd))
  result = execShell(cmd)

# equal command
proc eqCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call equal command => " & repr(cmd))
  let f = cmd.sadr.f
  var r = cmd.sadr.f.dot
  if r.p2 == -1: r.p2 = file.rfileSize(f)
  var pos = ""
  let l1 = if r.p1 == 0: 1 else: file.offsetToLine(r.p1, cmd.sadr.f)
  let l2 = if r.p2 == 0: 1 else: file.offsetToLine(r.p2 - 1, cmd.sadr.f)
  if l1 == l2:
    pos = pos & $l1 & ": "
  else:
    pos = pos & $l1 & ',' & $l2 & "; "
  if r.p1 == r.p2:
    pos = pos & '#' & $r.p1
  else:
    pos = pos & '#' & $r.p1 & ',' & '#' & $r.p2
  echo pos
  result = true

# cd command
proc cdCmd(cmd: parse.Cmd): bool =
  when DEBUG: debugLog(Module, "call cd command => " & repr(cmd))
  var dest: string
  if len(cmd.args) == 0:
    dest = getHomeDir()
  else:
    dest = cmd.args
    if dest[0] == '~': dest = unix.tilde(dest)
  result = true
  try:
    setCurrentDir(dest)
  except OSError:
    result = false

# z command(forth)
#proc zCmd(cmd: parse.Cmd): bool =
#  if len(cmd.args) == 0:
#    nforthLoop()
#    return true
#  else:
#    return nforth.nfDoEval("editor", 0, cmd.args)
    
proc addCmdHistory(cmd: parse.Cmd, odot: file.Range, count: int) =
  when DEBUG: debugLog(Module, "call add history: cmd => " & repr(cmd))
  let cmdm = (cmd: cmd.cmd,
              fname: cmd.sadr.f.fstat.name,
              dot: odot,
              count: count)
  cmdhistory.add(cmdm)
  inc(cmdhistptr)
  inc(cmdundoptr)

# test code
when isMainModule:
  import streams, times
  {.passL:"-liconv".}
#[
  block displayCommandTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    if file.loadFile(f): setCurFile(f)

    echo "** test display cmommand **"
    parse.inputLine("1,$p")
    cmd = parseCmd()
    #echo "command => ", repr(cmd)
    echo "ececute command '1,$p'=> ", execCmd(cmd)
    parse.inputLine("3p")
    cmd = parseCmd()
    echo "execute command '3p'=> ", execCmd(cmd)
    for i in 1..5:
      parse.inputLine("\n")
      cmd = parseCmd()
      echo "execute command 'nl'", i, " => ", execCmd(cmd)
    parse.inputLine("2\n")
    cmd = parseCmd()
    echo "execute command '2nl' => ", execCmd(cmd)
    parse.inputLine("=")
    cmd = parseCmd()
    echo "execute command '=' => ", execCmd(cmd)
    parse.inputLine("#20=")
    cmd = parseCmd()
    echo "execute command '#20=' => ", execCmd(cmd)
    parse.inputLine("5=")
    cmd = parseCmd()
    echo "execute command '5=' => ", execCmd(cmd)
    parse.inputLine("1,5=")
    cmd = parseCmd()
    echo "execute command '1,5=' => ", execCmd(cmd)

  block deleteTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)

    echo "** test delete command **"
    parse.inputLine("d")
    cmd = parseCmd()
    echo "execute command 'd' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("3 d")
    cmd = parseCmd()
    echo "execute command '3 d' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("5 d")
    cmd = parseCmd()
    echo "execute command '5 d' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u2")
    cmd = parseCmd()
    echo "execute command 'u2' => "
    if execCmd(cmd): file.writeAll(f)

  block appendTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)

    echo "** test append command **"
    parse.inputLine("a/NIMEDIT\n/")
    cmd = parseCmd()
    echo "execute command 'a/NIMEDIT\n/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("5a/ニムエディト\n/")
    cmd = parseCmd()
    echo "execute command '5a/ニムエディト\n/' => "
    if execCmd(cmd): file.writeAll(f)
    #parse.inputLine("a")
    #cmd = parseCmd()
    #echo "execute command 'a' Enter => "
    #if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u2")
    cmd = parseCmd()
    echo "execute command 'u2' => "
    if execCmd(cmd): file.writeAll(f)

  block insertChangeTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)

    echo "** test insert and change command **"
    parse.inputLine("2 i/NIMEDIT\n/")
    cmd = parseCmd()
    echo "execute command '2 i/NIMEDIT/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine(",#4 c/ニムエディト/")
    cmd = parseCmd()
    echo "execute command ',#4 c/ニムエディト/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u2")
    cmd = parseCmd()
    echo "execute command 'u2' => "
    if execCmd(cmd): file.writeAll(f)
]#
  block substitudeTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileLIst(f)
    if file.loadFile(f): setCurFile(f)

    echo "** test substitude command **"
    parse.inputLine(", s/Emacs/NIMEDIT/")
    cmd = parseCmd()
    echo "execute command ', s/Emacs/NIMEDIT/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("1,$ s2/Emacs/NIMEDIT/")
    cmd = parseCmd()
    echo "execute command '1,$ s2/Emacs/NIMEDIT/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine(",$ s/Emacs/NIMEDIT/g")
    cmd = parseCmd()
    echo "execute command ',$ s/Emacs/NIMEDIT/g' => "
    if execCmd(cmd):
      file.writeAll(f)
      #echo "f => ", repr(f)
    parse.inputLine(""", s/NIMEDIT/Oh, &, &, \&, &!/""")
    cmd = parseCmd()
    echo """execute command ', s/NIMEDIT/Oh, &, &, \&, &!/' => """
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u")
    cmd = parseCmd()
    echo "execute command 'u' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u-1")
    cmd = parseCmd()
    echo "execute command 'u-1' => "
    if execCmd(cmd): file.writeAll(f)
#[
  block copyTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    if file.loadFile(f): setCurFile(f)

    echo "** test copy command **"
    parse.inputLine("1 t 6")
    cmd = parseCmd()
    echo "execute command '1 t 6' => "
    if execCmd(cmd): file.writeAll(f)

  block moveTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)

    echo "** test move command **"
    parse.inputLine("1 m 6")
    cmd = parseCmd()
    echo "execute command '1 m 6' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u")
    cmd = parseCmd()
    echo "execute command 'u' => "
    if execCmd(cmd): file.writeAll(f)

  block loopTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)

    #echo "** test loop command **"
    #parse.inputLine(", x/Emacs/")
    #cmd = parseCmd()
    #echo "execute command ', x/Emacs/' => ", execCmd(cmd)
    #parse.inputLine(", x/Emacs/ p")
    #cmd = parseCmd()
    #echo "execute command ', x/Emacs/ p' => ", execCmd(cmd)
    #parse.inputLine(", x/Emacs/ d")
    #cmd = parseCmd()
    #echo "execute command ', x/Emacs/ d' => "
    #if execCmd(cmd): file.writeAll(f)
    #parse.inputLine(", y/Emacs/")
    #cmd = parseCmd()
    #echo "execute command ', y/Emacs/' => ", execCmd(cmd)
    #parse.inputLine(", y/Emacs/ d")
    #cmd = parseCmd()
    #echo "execute command ', y/Emacs/ d' => "
    #if execCmd(cmd): file.writeAll(f)
    #parse.inputLine(", x p")
    #cmd = parseCmd()
    #echo "execute command ', x p' => ", execCmd(cmd)
    parse.inputLine(", x s/^/#/")
    cmd = parseCmd()
    echo "execute command ', x s/^/#/' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("u")
    cmd = parseCmd()
    echo "execute command 'u' => "
    if execCmd(cmd): file.writeAll(f)
    #parse.inputLine(", g/Emacs/")
    #cmd = parseCmd()
    #echo "execute command ', g/Emacs/' => ", execCmd(cmd)
    #parse.inputLine(", v/Emacs/")
    #cmd = parseCmd()
    #echo "execute command ', v/Emacs/' => ", execCmd(cmd)

  block fileLooperText:
    type
      TestFiles = array[0 .. 2, string]
    var
      cmd: Cmd
      f: file.File
      files: TestFiles
    files =["test.txt", "file.nim", "address.nim"]
    for fn in items(files):
      f = file.initFile(fn)
      file.setFileList(f)

    f = file.getFileObj("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "initial file list => "
    showFileList()

    echo "** test file loop command **"
    parse.inputLine("""X/\W+\w+'.nim'/""")
    cmd = parseCmd()
    echo """execute command 'X/\W+\w+'.nim'/' => """, execCmd(cmd)
    parse.inputLine("X")
    cmd = parseCmd()
    echo "execute command 'X' => ", execCmd(cmd)
    file.setFileTogleClean(f)
    echo "file list => "
    showFileList()
    parse.inputLine("""X/\'/""")
    cmd = parseCmd()
    echo """execute command 'X/\'/' => """, execCmd(cmd)
    parse.inputLine("""Y/\W+\w+'.nim'/""")
    cmd = parseCmd()
    echo """execute command 'Y/\W+\w+'.nim'/' => """, execCmd(cmd)
    parse.inputLine("""Y/\W+\w+'.nim'/ D""")
    cmd = parseCmd()
    echo """execute command 'Y/\W+\w+'.nim'/ D' => """
    if execCmd(cmd): file.showFileList()

  block fileCommandTest:
    type
      TestFiles = array[0 .. 2, string]
    var
      cmd: Cmd
      f: file.File
      files: TestFiles
    files = ["test.txt", "piece.nim", "nimedit.nim"]
    for fn in items(files):
      f = file.initFile(fn)
      file.setFileList(f)

    f = file.getFileObj("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "initial file list => "
    showFileList()

    echo "** test file command **"
    parse.inputLine("n")
    cmd = parseCmd()
    echo "execute command 'n' => ", execCmd(cmd)
    parse.inputLine("b piece.nim nimedit.nim")
    cmd = parseCmd()
    echo "execute command 'b piece.nim nimedit.nim' => "
    if execCmd(cmd): showFileList()
    parse.inputLine("B file.nim parse.nim address.nim")
    cmd = parseCmd()
    echo "execute command 'B file.nim parse.nim address.nim' => "
    if execCmd(cmd): showFileList()
    parse.inputLine("D")
    cmd = parseCmd()
    echo "execute command 'D' => "
    if execCmd(cmd): showFileList()
    parse.inputLine("D file.nim parse.nim")
    cmd = parseCmd()
    echo "execute command 'D file.nim parse.nim' => "
    if execCmd(cmd): showFileList()

  block fileIOTest:
    type
      TestFiles = array[0 .. 2, string]
    var
      cmd: Cmd
      f: file.File
      files: TestFiles
    files = ["test.txt", "piece.nim", "nimedit.nim"]
    for fn in items(files):
      f = file.initFile(fn)
      file.setFileList(f)

    f = file.getFileObj("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "initial file list => "
    showFileList()
    echo "** test file IO comannd **"
    parse.inputLine("e foo.txt")

    cmd = parseCmd()
    echo "execute command 'e foo.txt' => "
    if execCmd(cmd): showFIleList()
    parse.inputLine("w out.txt")
    cmd = parseCmd()
    echo "execute command 'w out.txt' => "
    if execCmd(cmd): echo "out.txt => ", readFile("out.txt")
    parse.inputLine("f example.txt")
    cmd = parseCmd()
    echo "execute command 'f example.txt' => "
    if execCmd(cmd): showFileList()
    parse.inputLine("r out.txt")
    cmd = parseCmd()
    echo "execute command 'r out.txt' => ", execCmd(cmd)
    parse.inputLine("1,$p")
    cmd = parseCmd()
    echo "example.txt => ", execCmd(cmd)

  block shellCommandTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "** test shell comand **"
    parse.inputLine("! ls")
    cmd = parseCmd()
    echo "execute command '! ls' => ", execCmd(cmd)
    parse.inputLine("4 <date")
    cmd = parseCmd()
    echo "execute command '4 <date' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine(", >sort")
    cmd = parseCmd()
    echo "execute command ', >sort' => ", execCmd(cmd)
    parse.inputLine(", | sort")
    cmd = parseCmd()
    echo "execute command ', | sort' => "
    if execCmd(cmd): file.writeAll(f)
    parse.inputLine("cd")
    cmd = parseCmd()
    echo "execute command 'cd' => ", execCmd(cmd)
    parse.inputLine("!echo $PWD")
    cmd = parseCmd()
    echo "pwd => ", execCmd(cmd)
    parse.inputLine("cd ~/org")
    cmd = parseCmd()
    echo "execute command 'cd ~/org' => ", execCmd(cmd)
    parse.inputLine("!echo $PWD")
    cmd = parseCmd()
    echo "pwd => ", execCmd(cmd)

  block miscCommandTest:
    type
      TestFiles = array[0 .. 2, string]
    var
      cmd: Cmd
      f: file.File
      files: TestFiles
    files = ["test.txt", "piece.nim", "nimedit.nim"]
    for fn in items(files):
      f = file.initFile(fn)
      file.setFileList(f)

    f = file.getFileObj("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "initial file list => "
    showFileList()
    echo "** test misc command **"
    parse.inputLine("3 k")
    cmd = parseCmd()
    echo "execute command '3 k'"
    if execCmd(cmd): echo " mark => ", repr(f.mark)
    parse.inputLine(""""piece.nim" 3""")
    cmd = parseCmd()
    echo """execute command '"piece.nim" 3' => """
    if execCmd(cmd): showFileList()
    parse.inputLine("q")
    cmd = parseCmd()
    echo "execute command 'q' => "
    if execCmd(cmd): echo "file obj => ", repr(file.curfile)

  block mixCommandTest:
    var f: file.File
    var cmd: Cmd
    new(cmd)
    f = file.initFile("test.txt")
    if file.loadFile(f): setCurFile(f)
    echo "** test mix command **"
    #parse.inputLine("B <echo *.nim")
    #cmd = parseCmd()
    #echo "execute command 'B <echo *.nim' => "
    #if execCmd(cmd): file.showFileList()
    #parse.inputLine("B <grep -l PLAN9 *.nim")
    #cmd = parseCmd()
    #echo "execute command 'B <grep -l PLAN9 *.nim' => "
    #if execCmd(cmd): file.showFileList()
    #parse.inputLine("""0,$ t "piece.nim" 0""")
    #cmd = parseCmd()
    #echo """execute command '0,$ t "piece.nim" 0' => """
    #if execCmd(cmd):
    #  parse.inputLine("w output")
    #  cmd = parseCmd()
    #  echo execCmd(cmd)
    parse.inputLine(""", x/@\n/ g/Emacs/ p""")
    cmd = parseCmd()
    echo """execute command ', x/@\n/ g/Emacs/ p' =>""", execCmd(cmd)
    parse.inputLine(", s2/Emacs/NIMEDIT/")
    cmd = parseCmd()
    if execCmd(cmd):
      parse.inputLine(""", x/@\n/ g/Emacs/ v/NIMEDIT/ p""")
      cmd = parseCmd()
      echo """execute command ', x/@\n/ g/Emacs/ v/NIMEDIT/ p' => """, execCmd(cmd)

  block groupCommandTest:
    type
      TestFiles = array[0 .. 3, string]
    var
      cmd: Cmd
      f: file.File
      files: TestFiles
    files = ["test.txt", "file.nim", "parse.nim", "ned.nim"]
    for fn in items(files):
      f = file.initFile(fn)
      file.setFileList(f)

    f = file.getFileObj("test.txt")
    #f = file.getFileObj("parse.nim")
    if file.loadFile(f): setCurFile(f)
    parse.inputLine("""X/\W+\w+'.nim'/ ,g/buf/ {
                               f
                               , x/@\n/ g/buf/ p
                       }""")
    #parse.inputLine("""X/\W+\w+'.nim'/ ,g/buf/ f""")
    #parse.inputLine(""", x/@\n/ g/buf/ p""")
    cmd = parseCmd()
    echo "execute group command => ", execCmd(cmd)
    #echo "group command => ", repr(cmd)

  block timeTest:
    echo "time test start ..."
    var f = file.initFile("waldn10.txt")
    if file.loadFile(f): setCurFile(f)

    parse.inputLine(", x g/Thoreau/ p")
    #parse.inputLine(", x/Thoreau/")
    #parse.inputLine(", x")
    var cmd = parseCmd()
    let t0 = cpuTime()
    discard execCmd(cmd)
    echo "CPU time [s] => ", cpuTime() - t0

  block nforth:
    var
      cmd: Cmd
      f: file.File
    f = file.initFile("test.txt")
    echo "f => ", repr(f)
    file.setFileList(f)
    if file.loadFile(f): setCurFile(f)
    else: echo "loadFile error!"
    
    initNforth(false, "", ["forth/core.zf", "forth/dict.zf"])
    parse.inputLine("z")
    cmd = parseCmd()
    discard execCmd(cmd)

  block parseCmdTest:
    parse.inputLine("s5@abc@ABC@")
    var cmd = parseCmd()
    echo "parseCmd cmd => ", repr(cmd)
]#
