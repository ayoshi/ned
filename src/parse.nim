import unicode, strutils, sequtils    # rdstdin
import file, term

const DEBUG = true

const
  Addrp* = {ord('#'), ord(','), ord(';'), ord('+'), ord('-'), ord('/'), ord('?'), ord('.'), ord('\''), ord('"'), ord('$'), ord('0')..ord('9')}
  Bl* = {ord(' '), ord('\t')}
  Num* = {ord('0')..ord('9')}

type
  Address* = tuple[r: file.Range, f: file.File]
  Cmd* = ref CmdObj
  CmdObj* = object of RootObj
    cmd*: char
    sadr*: Address     # src address
    dadr*: Address     # dest address for m, t
    args*: string
    flag*: char        # g, v, X, Y defcmd: p, f
    next*: Cmd
    
var linebuf*: seq[Rune]
var linep* = 0   # linebuf index

# input line: string of line change into Runes sequence for test
proc inputLine*(line: TaintedString) =
  linebuf = toRunes(line & '\n')
  linep = 0

# get char = int
proc getc*(): int =
  if linep <= len(linebuf) - 1:
    let r = linebuf[linep]
    result = ord(r)
    inc(linep)
  else:
    #inputLine()
    result = -1

# next char
proc nextc*(): int =
  if linep <= len(linebuf) - 1:
    let r = linebuf[linep]
    result = ord(r)
  else: result = -1
  
# unget char
proc ungetc*() =
  if linep > 0:
    dec(linep)
  else:
    echo "panic ungetChar"    # panic: rescue and abort

# skip Bl = {' ', '\t'}
proc skipBl*(): int =
  var c: int
  c = getc()
  while c in Bl:
    c = getc()
  if c >= 0:
    ungetc()
  result = c
      
# get number string
proc getNum*(): string =
  var c: int
  result = ""
  while true:
     c = getc()
     if c notin Num: break
     if c < 128:
       result.add(chr(c))
     # else:
     #   ungetChar()
     #   result = result & toUTF8(linebuf[linep])
     #   discard getChar()

proc okDelim*(c: int): bool =
  result = (c == ord('\\') or c notin {ord('a')..ord('z'), ord('A')..ord('Z'), ord('0')..ord('9')})

proc getRegexp*(delim: int, raw: bool = false): string =
  var c, prev: int
  result = ""
  while true:
    c = getc()
    if c == delim and prev != ord('\\'):
      break
    else: prev = c
    if c == -1:
      echo "No found right delimitter: ", delim
      result = ""
      break
    if c < 128:
      if raw:
        if c == ord('\\') and nextc() == ord('n'):    # "\n" => newline '\n'
          discard getc()
          prev = ord('n')
          result.add('\n')
        else:
          result.add(chr(c))
      else:
        result.add(chr(c))
    else:
      ungetc()
      result.add(toUTF8(linebuf[linep]))
      discard getc()

proc getText*(until: int): string =
  getRegexp(until)
  
proc getInput*(skk: bool = false): string =
  var input: seq[Rune]
  var line = ""
  result = ""
  while true:
    input = term.lineEdit(skk = skk)
    #when DEBUG: echo "input => ", repr(input)
    stdout.write('\n')
    if input.len != 0 and toUTF8(input[0]) == ".": break
    for r in items(input): line.add(toUTF8(r))
    result.add(line & '\n')
    line = ""
  #when DEBUG: echo "result => ", result

# test code
when isMainModule:
  {.passL:"-liconv".}
  block test1:
    var ch: int
    #inputLine("s/t/st/")
    inputLine("s@t@st@")
    echo "skipBl => ", skipBl()
    discard getc()
    echo "skipBl => ", skipBl()

    #ungetc()
    while true:
      ch = getc()
      if ch == -1: break
      if ch < 128:
        write(stdout, chr(ch))
      else:
        ungetc()
        write(stdout, toUTF8(linebuf[linep]))
        discard getc()
    write(stdout, '\n')

    #echo "Enter number => "
    inputLine("123")
    echo "linebuf => ", repr(linebuf)
    while true:
      ch = getc()
      if ch in Num: break
    ungetc()
    echo "number string => ", getNum()

    echo "okDelim(ord('@')) => ", if okDelim(ord('@')): "@ is OK" else: "@ is NO"

    inputLine("/こんにちわ　hello, world! 世界！/")
    ch = getc()
    if okDelim(ch):
      echo "regexp => ", getRegexp(ord(ch))

  block test2:
    echo "Enter =>"
    echo "Your input = > ", getInput()

  block test3:
    inputLine("This is a pen.")
    echo "getText => ", getText(ord('\n'))
    
