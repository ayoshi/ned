# separated from command.nim

proc display(r: file.Range, f: var file.File): bool =
  when DEBUG: debugLog(Module, "range => " & repr(r))
  let s = file.snapshot(f)
  let p1 = address.indexToOffset(s, r.p1, f)
  let p2 = address.indexToOffset(s, r.p2, f)
  var i = p1
  #while i < p2:
  #  write(stdout, s[i])
  #  inc(i)
  if curClient == nil:
    write(stdout, s[p1..p2 - 1])
    file.setDot(r.p1, r.p2, f)
  else:
    if r.p2 == -1:
      server.send(curClient, "RANGE: " & $r.p1 & "," & $runelen(s))
    else:
      server.send(curClient, "RANGE: " & $r.p1 & "," & $r.p2)
  result = true

# copy sadr to dadr
proc copy(a1, a2: var parse.Address) =
  let s1 = file.snapshot(a1.f)
  let a1rp1 = address.indexToOffset(s1, a1.r.p1, a1.f)
  let a1rp2 = if a1.r.p2 == -1: s1.len else: address.indexToOffset(s1, a1.r.p2, a1.f)
  let src = s1[a1rp1..a1rp2 - 1]
  var a2rp2: int
  if a1.f != a2.f:
    let s2 = file.snapshot(a2.f)
    a2rp2 = if a2.r.p2 == -1: s2.len else: address.indexToOffset(s2, a2.r.p2, a2.f)
  else:
    a2rp2 = if a2.r.p2 == -1: s1.len else: address.indexToOffset(s1, a2.r.p2, a2.f)
  file.appendStr(a2rp2, src, a2.f)
  if a2.r.p2 == -1: a2.r.p2 = file.rfileSize(a2.f)
  file.setDot(a2.r.p2, a2.r.p2 + runeLen(src), a2.f)

# move sadr to dadr
proc move(a1, a2: var parse.Address) =
  let s1 = file.snapshot(a1.f)
  let a1rp1 = address.indexToOffset(s1, a1.r.p1, a1.f)
  let a1rp2 = address.indexToOffset(s1, a1.r.p2, a1.f)
  let src = s1[a1rp1..a1rp2 - 1]
  if a1.f != a2.f:
    let s2 = file.snapshot(a2.f)
    let a2rp2 = address.indexToOffset(s2, a2.r.p2, a2.f)
    file.appendStr(a2rp2, src, a2.f)
    file.deleteStr(a1rp1, src.len, a1.f)
  else:
    let a2rp2 = address.indexToOffset(s1, a2.r.p2, a2.f)
    if a1.r.p2 <= a2.r.p2:
      file.appendStr(a2rp2, src, a2.f)
      file.deleteStr(a1rp1, src.len, a1.f)
    elif a1.r.p1 >= a2.r.p2:
      file.deleteStr(a1rp1, src.len, a1.f)
      file.appendStr(a2rp2, src, a2.f)
  file.setDot(a2.r.p2, a2.r.p2 + runeLen(src), a2.f)

# looper: x, y looping
proc looper(cmd: parse.Cmd): bool =
  var f = cmd.sadr.f
  var r = cmd.sadr.r
  var s: string
  shallowCopy(s, file.snapshot(f))   # TODO
  let re = cmd.args[1..high(cmd.args)]
  var p0 = address.indexToOffset(s, r.p1, f)
  var p3 = if r.p2 == -1: s.len - 1 else: address.indexToOffset(s, r.p2, f)
  var prevlen, delta: int
  var cnt = 0
  let odot = f.dot

  if (p0 == 0 and p3 == 0) or p3 > s.len:       # file.fileSize(f):
    p3 = s.len - 1                                  # file.fileSize(f)
  when DEBUG: debugLog(Module, "looper p0 => " & $p0 & " p3 => " & $p3)
  inc(nest)
  while true:
    if search.execute(f, p0, s[p0..p3], re):
      let (p1, p2) = f.sel[0]
      case cmd.cmd
      of 'x':
        if cmd.flag == 'p':
           result = display((address.offsetToIndex(s, p1, f),
                             address.offsetToIndex(s, p2 + 1, f)), f)
        else:
          if cmd.next != nil:
            let ip1 = address.offsetToIndex(s, p1, f)
            let ip2 = address.offsetToIndex(s, p2 + 1, f)
            file.setDot(ip1, ip2, f)
            cmd.next.sadr.r = (ip1, ip2)
            cmd.next.sadr.f = f
            result = execCmd(cmd.next)
          else: result = false
        if result:
          prevlen = s.len
          shallowCopy(s, file.snapshot(f))    # TODO
          delta = s.len - prevlen
          p0 = p2 + delta + 1
          p3 += delta
          if p0 < 0: p0 = 0
          if p3 > s.len or p0 > p3: break
        else:
          p0 = p2 + 1
          if p0 > s.len: break
        if len(f.sel) != 0: f.sel.delete(0, high(f.sel))
      of 'y':
        if cmd.flag == 'p':
          result = display((address.offsetToIndex(s, p0, f),
                            address.offsetToIndex(s, p1, f)), f)
        else:
          if cmd.next != nil:
            let ip0 = address.offsetToIndex(s, p0, f)
            let ip1 = address.offsetToIndex(s, p1, f)
            file.setDot(ip0, ip1, f)
            cmd.next.sadr.r = (ip0, ip1)
            cmd.next.sadr.f = f
            result = execCmd(cmd.next)
          else: result = false
        if result:
          prevlen = s.len
          shallowCopy(s, file.snapshot(f))
          delta = s.len - prevlen
          p0 = p2 + delta + 1
          p3 += delta
          if p0 < 0: p0 = 0
          if p3 > s.len or p0 > p3: break
        else:
          p0 = p2 + 1
          if p0 > s.len: break
        if len(f.sel) != 0: f.sel.delete(0, high(f.sel))
      else: break
    else:
      if cmd.cmd == 'y':
        if cmd.flag == 'p':
          result = display((address.offsetToIndex(s, p0, f),
                            address.offsetToIndex(s, p3 - 1, f)), f)
          break
        else:
          if cmd.next != nil:
            let ip0 = address.offsetToIndex(s, p0, f)
            let ip3 = address.offsetToIndex(s, p3, f)
            file.setDot(ip0, ip3, f)
            cmd.next.sadr.r = (ip0, ip3)
            cmd.next.sadr.f = f
            result = execCmd(cmd.next)
          break
      else: break
    inc(cnt)     # loop command history count
  dec(nest)
  if cmd.next != nil: addCmdHistory(cmd.next, odot, cnt)

# line looper
#[
proc linelooper(cmd: parse.Cmd): bool =
  var f = cmd.sadr.f
  var s = file.snapshot(f)
  let r = cmd.sadr.r
  var p0 = if r.p1 == 0: 0 else: address.indexToOffset(s, r.p1, f)
  var p3 = if r.p2 == -1 or r.p2 == file.rfileSize(f): s.len - 1
           else: address.indexToOffset(s, r.p2, f)
  var prevlen, delta: int
  var cnt = 0
  let odot = cmd.sadr.f.dot

  inc(nest)
  for l in splitLines(s[p0..p3], keepEol = true):
    let ip0 = address.offsetToIndex(s, p0, f)
    let rl = runeLen(l)
    if cmd.flag == 'p':
      result = display((ip0, ip0 + rl), f)
    else:
      if cmd.next != nil:
        file.setDot(ip0, ip0 + rl, f)
        cmd.next.sadr.r = (ip0, ip0 + rl)
        cmd.next.sadr.f = f
        result = execCmd(cmd.next)
    if result or (cmd.next != nil and cmd.next.cmd in {'g', 'v'}):
      if cmd.next != nil and cmd.next.cmd in {'a', 'i', 'c', 'd', 's', 't'}:
        prevlen = s.len
        s = file.snapshot(f)
        delta = s.len - prevlen
        p3 += delta
      p0 += l.len
      when DEBUG: debugLog(Module, "linelooper delta: " & $delta & " p0: " & $p0 & " p3: " & $p3)
    else: break
    let ip3 = if p3 == s.len - 1: file.rfileSize(f) else: address.offsetToIndex(s, p3, f)
    file.setDot(ip0, ip3 + 1, f)
    inc(cnt)      # loop command history count
  dec(nest)
  if cmd.next != nil: addCmdHistory(cmd.next, odot, cnt)
]#  
proc linelooper(cmd: parse.Cmd): bool =
  var f = cmd.sadr.f
  var r = cmd.sadr.r
  #var sr = file.rsnapshot(f)
  var rsize = file.rfileSize(f)
  if r.p2 == -1 or r.p2 == rsize: r.p2 = rsize - 1

  var p1, p2: int
  var ln = file.offsetToLine(r.p1, f)
  (p1, p2) = file.lineToRange(ln, f)
  let last = file.lastLine(f)

  var prevlen, delta: int
  var cnt = 0
  let odot = cmd.sadr.f.dot

  inc(nest)
  var rcnt: int
  while ln < last:
    if cmd.flag == 'p':
      result = display((p1, p2), f)
    else:
      if cmd.next != nil:
        file.setDot(p1, p2, f)
        cmd.next.sadr.r = (p1, p2)
        cmd.next.sadr.f = f
        result = execCmd(cmd.next)
    inc(ln)
    (p1, p2) = file.lineToRange(ln, f)
    rsize = file.rfileSize(f)
    r.p2 = rsize - 1
    file.setDot(r.p1, r.p2 + 1, f)
    inc(cnt)      # loop command history count
  dec(nest)
  if cmd.next != nil: addCmdHistory(cmd.next, odot, cnt)

# shell
proc execShell(cmd: parse.Cmd): bool =
  let path = if existsDir(PLAN9DIR): PLAN9DIR & "/bin/" else: ""
  var cmdpath = if len(path) != 0: path & cmd.args else: cmd.args
  var rp1, rp2: int

  result = true
  case cmd.cmd
  of '!':
    let (outp, errC) = execCmdEx(cmdpath)
    stdout.write(outp)
    if errC != 0: result = false
  of '<':
    let (outp, errC) = execCmdEx(cmdpath)
    if errC != 0: result = false
    else:
      var f = cmd.sadr.f
      var a = cmd.sadr
      let s = file.snapshot(f)
      if a.r.p1 == -1:
        a.r.p1 = 0
        a.r.p2 = file.rfileSize(f)
        rp1 = 0
        rp2 = s.len
      elif a.r.p2 == -1:
        a.r.p2 = file.rfileSize(f)
        rp1 = address.indexToOffset(s, a.r.p1, f)
        rp2 = s.len
      else:
        rp1 = address.indexToOffset(s, a.r.p1, f)
        rp2 = address.indexToOffset(s, a.r.p1, f)
      file.replaceStr(rp1, rp2 - rp1, outp, f)
      file.setDot(a.r.p1, a.r.p1 + runeLen(outp), f)
  of '>', '|':
    var f = cmd.sadr.f
    var a = cmd.sadr
    let s = file.snapshot(f)
    if a.r.p1 == -1:
      a.r.p1 = 0
      a.r.p2 = file.rfileSize(f)
      rp1 = 0
      rp2 = s.len
    elif a.r.p2 == -1:
      a.r.p2 = file.rfileSize(f)
      rp1 = address.indexToOffset(s, a.r.p1, f)
      rp2 = s.len
    else:
      rp1 = address.indexToOffset(s, a.r.p1, f)
      rp2 = address.indexToOffset(s, a.r.p2, f)
    let start = rp1
    let endp = if rp2 == s.len: rp2 - 1 else: rp2
    let user = unix.getUser()
    let tmpdir = getTempDir() & unix.PNAME & '_' & user
    let tmpfile = tmpdir & "/data"
    var sf: system.File
    if open(sf, tmpfile, fmWrite):
      write(sf, s[start .. endp])
      flushFile(sf)
    let (outp, errC) = execCmdEx(cmdpath & ' ' & tmpfile)
    if errC == 0:
      if cmd.cmd == '>': stdout.write(outp)
      else:
        file.replaceStr(rp1, rp2 - rp1, outp, f)
        file.setDot(a.r.p1, a.r.p1 + runeLen(outp), f)
    else: echo "error"
    removeFile(tmpfile)
  else:
    result = false
  echo "!"

# replace & with match string: s/Peter/Oh, & & \& &/ => Oh, Peter Peter & Peter
proc replaceAmpersant(s, by: string): string =
  var first, last = 0
  result = ""
  while last < s.len - 1:
    while s[last] notin {'&', '\0'}: inc(last)
    if s[last] == '&':
      if s[last - 1] != '\\':
        result = result & s[first..last - 1] & by
      else:
        result = result & s[first..last - 2] & '&'
    inc(last)
    first = last
  result = result & s[first..last]

proc sendFile*(client: Socket, f: file.File) =
  let s = file.snapshot(f)
  
  server.send(client, "CONTENTS: " & $file.fileSize(f))
  server.send(client, s)

