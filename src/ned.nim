import os, parseopt, unicode, sequtils            #, nimprof
import file, parse, term, server
from command import quitok, parseCmd, execCmd

# forth
include nfmain

const DEBUG = false

{.passL: "-liconv".}

const
  Version = "ned 0.1.0"
  HelpMsg = """
    ned [ options ...] [ files ]
    -a: autoindent(not implement)
    -d: do not download gui part
    -h: show this message
    -j: skk mode
    -v: show version"""

var
  skkflag = false     # lineEdit with skkon
  quitFlag = false
  download = true
  
proc bye() =
  echo "bye"
  quit()
  
proc showHelp() =
  echo HelpMsg
  bye()

proc showVersion() =
  echo Version
  bye()

proc readLine*(prompt: string, skk: bool = false) =
  let input = term.lineEdit(prompt, skk = skk)
  parse.linebuf = concat(input, toRunes("\n"))
  parse.linep = 0
  when DEBUG:
    echo "input => ", repr(parse.linebuf)
  stdout.write('\n')
  term.addIHistory(input)

# Forth handler
proc handleForth(line: seq[Rune]) =
  var s = ""
  if line.len == 2:
    nforthLoop()
  else:
    for i in 1..high(line) - 1: s.add(toUTF8(line[i]))
    s.add('\0')
    discard nforth.nfEval(s)

# Gui part handler which is argument for nedServer
proc handleGui(client: Socket, data: string) =
  var cmd: Cmd

  # Debug code
  #if data.len != 0:
  #  echo "Received data => ", repr(data)
  #  server.send(client, data)
  if data.len != 0:
    if data == "hi":      # hand shake and send current file content
      server.send(client, "hi")
      if file.curfile != nil:
        command.sendFile(client, file.curfile)
    elif data[0..2] == "SKK" and len(data) == 4:
      case data[3]
        of "M":    # change skk mode
          echo "change skk mode on => ", data
        of "@":    # toggle Hira, Kata
          echo "toggle hira, kata => ", data
        of "*":    # Ascii, Zenei to Hira
          echo "ascii, zenei to Hira => ", data
        of "L":    # Hira, Kata to Zenei
          echo "Hira, Kata to Zenei => ", data
        else: discard
    elif data[0..3] == "KANA":    # Hira, Kata or Zenei conversion
      echo "Hira, Kata conversion => ", data
    elif data[0..3] == "YOMI":    # Kanji conversion
      echo "Kanji conversion => ", data
    else:
      parse.inputLine(data)
      cmd = parseCmd()
      if cmd != nil:
        if execCmd(cmd):     # TODO: pass client as arguments
          if cmd.cmd == 'q': quitFlag = command.quitok
      
proc mainLoop() =
  var cmd: Cmd
  
  if download:
    echo "Ned gui mode"
    #let outp = execProcess("nedgui")    # TODO: error handling
    let (client, _) = server.nedServer(handleGui)
    command.curClient = client
    while true:
      if quitFlag: break
  else:
    while not quitFlag:
      readLine(prompt = "", skk = false)
      let fst = toUTF8(parse.linebuf[0])
      #echo "mainLoop fst => ", fst
      if fst[0] notin {'z', 'Z'}:
        cmd = if skkflag: parseCmd(true) else: parseCmd()
        if cmd == nil: continue
        if execCmd(cmd):
          # if qCmd return true, quitFlag is true
          if cmd.cmd == 'q': quitFlag = command.quitok
      else:
        handleForth(parse.linebuf)

proc main() =
  var
    filenames = newSeq[string]()
    f: file.File
  # opt
  when declared(commandLineParams):
    var p = initOptParser(commandLineParams())
  else:
    var p = initOptParser("--help -d -j -v")

  for kind, key, val in p.getopt():
    case kind
    of cmdArgument:
      filenames.add(key)
    of cmdLongOption, cmdShortOption:
      case key
      of "help", "h": showHelp()
      of "download", "d": download = false
      of "japanese", "j": skkflag = true
      of "version", "v": showVersion()
    of cmdEnd: assert(false)  # cannot happen
  if filenames.len == 0:
    # no filenmae has been given
    f = file.initFile()
    if f != nil: file.setFileList(f)
    if file.loadFile(f): file.setCurFile(f)
  else:
    for fname in items(filenames):
      f = file.initFile(fname)
      if f != nil: file.setFileList(f)
    f = file.getFileObj(filenames[0])
    if file.loadFile(f): file.setCurFile(f)
  for fi in items(file.filelist):
    echo fi.fstat.name, ": ", fi.fstat.size
    
  # main loop
  mainLoop()
  bye()

main()

